<?php
/**
 * Global bootstrap file.
 */

// Security settings
define('DF_IS_COMMAND_LINE', (PHP_SAPI == "cli"));
define("DF_IS_SECURE", (DF_IS_COMMAND_LINE || (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on")) ? TRUE : FALSE);

define("DF_INCLUDE_BASE", dirname(__FILE__));
define("DF_INCLUDE_ROOT", realpath(DF_INCLUDE_BASE.'/..'));

define("DF_INCLUDE_APP", DF_INCLUDE_BASE.'/app');
define("DF_INCLUDE_MODULES", DF_INCLUDE_BASE.'/modules');
define("DF_INCLUDE_MODELS", DF_INCLUDE_BASE.'/models');
define("DF_INCLUDE_STATIC", DF_INCLUDE_BASE.'/static');
define("DF_INCLUDE_TEMP", DF_INCLUDE_ROOT.'/tmp');
define("DF_INCLUDE_CACHE", DF_INCLUDE_TEMP.'/cache');

define("DF_INCLUDE_LIB_LOCAL", DF_INCLUDE_BASE.'/library');
define("DF_INCLUDE_LIB", DF_INCLUDE_BASE.'/../../libraries');
define("DF_UPLOAD_FOLDER", DF_INCLUDE_STATIC);

// Self-reference to current script.
define("DF_THIS_PAGE", reset(explode("?", $_SERVER['REQUEST_URI'])));
define("DF_TIME", time());

// Application environment.
if (!defined('DF_APPLICATION_ENV'))
    define('DF_APPLICATION_ENV', ($env = @file_get_contents(DF_INCLUDE_ROOT . DIRECTORY_SEPARATOR . '.env')) ? trim($env) : 'development');

// Set error reporting.
if (DF_APPLICATION_ENV != 'production')
    error_reporting(E_ALL & ~E_NOTICE);
else
    error_reporting(E_ERROR & E_PARSE & ~E_NOTICE);

// Set include path (as needed by CLI access.)
$include_path = array(DF_INCLUDE_LIB_LOCAL, DF_INCLUDE_LIB, get_include_path());
set_include_path(implode(PATH_SEPARATOR, $include_path));

// Save configuration object.
require('DF/Config.php');
$config = new DF_Config(DF_INCLUDE_APP.'/config');
$config->preload('general');

// Initialize the ZendFramework Application Bootstrapper.
require('Zend/Application.php');
$application = new Zend_Application('application', $config->application);
$application->getBootstrap();

// Save the configuration object to the global registry.
Zend_Registry::set('application', $application);
Zend_Registry::set('config', $config);
Zend_Registry::set('cache', new DF_Cache());