<?php
/**
 * Synchronization file
 */

require_once dirname(__FILE__).'/bootstrap.php';

error_reporting(E_ALL);
set_time_limit(0);

$application->bootstrap();

$sync_type = (isset($_REQUEST['type'])) ? $_REQUEST['type'] : 'hourly';

$current_hour = intval(date('G'));
$current_minute = intval(date('i'));
$current_hour_code = ($current_minute <= 30) ? $current_hour : $current_hour + 1;
$current_hour_code = ($current_hour_code == 24) ? 0 : intval($current_hour_code);

$current_day = date('j');
$current_month = date('m');
$days_in_month = date('t');

UA_BillingManager::generateLateFees();
echoToShell(' - POSTED LATE FEES');

switch($current_hour_code)
{
	case 6:
		// EMS/FAMIS synchronization.
        UA_FamisManager::generateDailyReport();
        echoToShell(' - SENT FAMIS/TOUCHNET SYNCHRONIZATION');
	break;
	
    case 7:
        UA_BillingManager::generateRecurringCharges();
        echoToShell(' - POSTED RECURRING CHARGES');
    break;
    
    case 8:
        // Synchronize GP2 residents.
        $return_stats = UA_ImportManager::syncExternalResidents();
        echoToShell(' - SYNCHRONIZED EXTERNAL RESIDENTS');
    break;

    case 9:
        // Special processing for fiscal year switch.
        $base_timestamp = NULL;
        
        if ($current_month == 9 && $current_day == 1)
            $base_timestamp = strtotime("-1 week");
        else if ($current_month != 8 && $current_day == ($days_in_month - 1))
            $base_timestamp = time();
        
        if ($base_timestamp !== NULL)
        {   
            UA_BillingManager::generateRentCharges($base_timestamp);
            echoToShell(' - GENERATED RENT CHARGES');

            // Immediately post charges for this apartment.
            UA_ElectricManager::generateCharges(NULL, FALSE);
            echoToShell(' - GENERATED ELECTRIC CHARGES');
            
            // Write billing manager log.
            $billing_log = UA_BillingManager::getLog();
            file_put_contents(DF_INCLUDE_TEMP.'/BillingLog-'.date('Ymd').'.txt', implode("\n", $billing_log));
        }
        
        UA_ReportManager::generateTransportationReport();
        echoToShell(' - GENERATED TRANSPORTATION REPORT');
    break;
}

echoToShell('SUCCESS');

function echoToShell($string)
{
	echo $string.PHP_EOL;
}