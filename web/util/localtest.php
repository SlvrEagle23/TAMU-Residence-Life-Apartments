<?php
/**
 * Local Test File
 */

require_once dirname(__FILE__).'/bootstrap.php';

$application->bootstrap();

$user = DF_Auth::getInstance()->getLoggedInUser();

foreach($user->Resident->Applications as $app)
{
    if ($app->Status->id == 1) // status 1 = Waiting
    {
        $fee = new ApplicationFee();
        $fee->fee_amount = $TNsplit->split_amount;
        $fee->fee_date = DF_TIME;
        $fee->fee_receipt = 'UAPTS466BFA5000003';
        $fee->application_id = $app->id;
        $fee->save();

        $app->application_fee_id = $fee->id;
        $app->save();
    }
}

exit;

$report = UA_ElectricManager::generateCharges();
DF_Utilities::print_r($report);

echo 'Done';