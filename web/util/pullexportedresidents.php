<?php
/**
 * Synchronization file
 */

require_once dirname(__FILE__).'/bootstrap.php';

$application->bootstrap();

$return_stats = UA_ImportManager::syncExternalResidents();

DF_Utilities::print_r($return_stats);
exit;