<?php
namespace DF\Application\Resource;
class Menu extends \Zend_Application_Resource_ResourceAbstract
{
    public function init()
    {
        $menu = array();
        foreach(new \DirectoryIterator(DF_INCLUDE_MODULES) as $item)
        {
            if( $item->isDir() && !$item->isDot() )
            {
                $menu_file = $item->getPathname().DIRECTORY_SEPARATOR.'menu.php';
                if(file_exists($menu_file))
                {
					$new_menu = (array)include_once($menu_file);
					$menu = $this->_mergeFlat($menu, $new_menu);
				}
            }
        }
		
		return new \DF\Menu($menu);
    }
    
    protected function _mergeFlat()
    {
		$arrays = func_get_args();
        $base = array_shift($arrays);

        foreach ($arrays as $array)
        {
            reset($base); //important
            foreach($array as $key => $value)
            {
                if (is_array($value) && @is_array($base[$key]))
                    $base[$key] = $this->_mergeFlat($base[$key], $value);
                else
                    $base[$key] = $value;
            }
        }

        return $base;
    }
}