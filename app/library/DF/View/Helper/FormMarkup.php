<?php
namespace DF\View\Helper;
class FormMarkup extends \Zend_View_Helper_FormElement
{
    public function formMarkup($name, $value = null, $attribs = null)
    {
		$info = $this->_getInfo($name, $value, $attribs);
        extract($info); // name, value, attribs, options, listsep, disable
        
        return $attribs['markup'];
	}
}