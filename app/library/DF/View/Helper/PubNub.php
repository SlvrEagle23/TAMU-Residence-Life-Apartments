<?php
namespace DF\View\Helper;
class PubNub extends \Zend_View_Helper_Abstract
{
	public function pubNub()
    {
		static $is_attached;
		
		if (!$is_attached)
		{
			$config = \Zend_Registry::get('config');
			$settings = $config->services->pubnub->toArray();

			$ssl = (DF_IS_SECURE) ? 'on' : 'off';

			echo '<div pub-key="'.$settings['pub_key'].'" sub-key="'.$settings['sub_key'].'" ssl="'.$ssl.'" origin="pubsub.pubnub.com" id="pubnub"></div>';
			echo '<script type="text/javascript" src="'.\DF\Url::content('jquery/addons/pubnub.min.js').'"></script>';
			
			$is_attached = TRUE;
		}
    }
}