<?php
namespace DF\Filter;
class WebAddress implements \Zend_Filter_Interface
{
    public function filter($value)
    {
		if ($value && substr($value, 0, 4) == "http")
			return 'http://'.trim(str_replace('http://', '', $value));
		else
			return trim($value);
    }
}