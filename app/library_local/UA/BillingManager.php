<?php
/**
 * University Apartments billing manager class.
 */

class UA_BillingManager
{
	/**
	 * Generate the rent charges for the upcoming month.
	 */
	public static function generateRentCharges($base_timestamp = NULL)
	{
		// Allow for calling this function with a custom timestamp.
		$base_timestamp = (is_null($base_timestamp)) ? time() : (int)$base_timestamp;
		$base_month = date('m', $base_timestamp);
		$base_year = date('Y', $base_timestamp);
		
		$start_of_month = mktime(0, 0, 0, $base_month+1, 1, $base_year);
		$middle_of_month = mktime(12, 0, 0, $base_month+1, 15, $base_year);
		$end_of_month = mktime(12, 0, 0, $base_month+2, 0, $base_year);
		
		// Get all assets that are up for automated billing.
		$all_assets = Asset::fetchAll();
		$rent_item = Item::fetchByName('Rent');
		
		foreach($all_assets as $asset)
		{
			set_time_limit(60);
			self::writeLog('[Rent] Asset '.$asset->name.' (ID '.$asset->id.') billing...');
			
			$prorate_info = self::proRateForTimeRange($asset, $middle_of_month, $start_of_month, $end_of_month);
			
			// Check for existing charges.
			foreach((array)$prorate_info['totals'] as $resident_id => $amount)
			{
				$existing_fee = Doctrine_Query::create()
					->from('Split s')->leftJoin('s.Transaction t')
					->addWhere('t.transaction_type = ?', RegisterTransaction::TRANSACTION_TYPE_RENT)
					->addWhere('s.asset_id = ?', $asset->id)
					->addWhere('s.resident_id = ?', $resident_id)
					->addWhere('s.due_date >= ?', $start_of_month)
					->addWhere('s.due_date < ?', $end_of_month)
					->fetchOne();
				
				if ($existing_fee)
				{
					unset($prorate_info['totals'][$resident_id]);
				}
			}
			
			if (!empty($prorate_info['totals']))
			{
				self::writeLog('[Rent] Asset '.$asset->name.' (ID '.$asset->id.') has prorated charges.');
				
				// Post new transaction.
				$transaction = new RegisterTransaction();
				$transaction->posted = time();
				$transaction->date = time();
				$transaction->transaction_type = RegisterTransaction::TRANSACTION_TYPE_RENT;
				$transaction->asset_id = $asset->id;
				$transaction->memo = 'Rent Charges - '.date('F Y', $middle_of_month).' for '.$asset->name;
				$transaction->save();
				$transaction_id = $transaction->id;
				
				foreach($prorate_info['totals'] as $resident_id => $amount)
				{
					if ($amount == 0)
						continue;
					
					// Post new split for this resident.
					$split = new Split();
					$split->posted = time();
					$split->transaction_id = $transaction_id;
					$split->resident_id = $resident_id;
					$split->ledger_id = Split::LEDGER_STANDARD;
					$split->credit_or_debit = 'D';
					$split->item_id = $rent_item->id;
					$split->asset_id = $asset->id;
					$split->due_date = ($prorate_info['due_dates'][$resident_id]) ? (int)$prorate_info['due_dates'][$resident_id] : (int)$start_of_month;
					$split->split_amount = 0-$amount;
					
					try
					{
						$split->save();
					}
					catch(Exception $e)
					{
						self::writeLog('[Rent] Posting Failed: '.$asset->name."\n".print_r($split->toArray(), TRUE));
					}
					
					Split::updateRunningBalance($split->resident_id, $split->ledger_id);
					
					$resident = $prorate_info['residents'][$resident_id];
				}
			}
			else
			{
				self::writeLog('[Rent] Asset '.$asset->name.' (ID '.$asset->id.') has no prorated charges.');
			}
		}
	}


	
	/**
	 * Pro-rate an amount across a given time range.
	 * @return An array of resident IDs and their associated amounts.
	 */
	public static function proRateForTimeRange($asset, $range_middle, $range_start, $range_end)
	{
		$asset_id = $asset->id;

		// Number of days to lock all pro-rating to.
		$num_days = 30;
		
		// Get array of days.
		$days = array();
		$k = 1;
		for($i = $range_start; $i <= $range_end; $i += 86400)
		{
			$days[$k] = $i;
			$k++;
		}
		$days = array_splice($days, 0, $num_days);
		
		if (count($days) < $num_days)
		{
			$last_day = array_pop($days);
			$days = array_pad($days, $num_days, $last_day);
		}
		
		// Get all occupancies that are active within the specified time range.
		$occupancies_in_range = Doctrine_Query::create()
			->from('Occupancy o')
			->leftJoin('o.Resident r')
			->addWhere('o.asset_id = ?', $asset_id)
			->addWhere('o.start_date <= ?', (int)$range_end)
			->addWhere('(o.end_date IS NULL OR o.end_date >= ? OR o.end_date = 0)', (int)$range_start)
			->orderBy('o.id DESC')
			->execute();
		
		if ($occupancies_in_range->count() == 0)
			return;
		
		$return = array(
			'days_per_resident' => array(),
			'residents'     => array(),
			'due_dates'     => array(),
			'costs'         => array(),
			'totals'        => array(),
		);

		// Billing configuration.
		$model = $asset->AssetType->OccupancyModel;
		$return['billing_type'] = ($model instanceof OccupancyModel) ? strtolower($model->name) : 'apartment';
		
		// Loop through 
		foreach($occupancies_in_range as $occupant)
		{
			$return['residents'][$occupant['resident_id']] = $occupant['Resident'];
			
			if ($occupant['start_date'] >= $range_start)
				$due_date = $occupant['start_date'];
			else
				$due_date = $range_start;
			
			$return['days_per_resident'][$occupant['resident_id']] = 0;
			$return['due_dates'][$occupant['resident_id']] = $due_date;
			$return['costs'][$occupant['id']] = $occupant->getCost($range_middle);
		}
		
		foreach($days as $i)
		{
			$occupants_for_day = array();
			foreach($occupancies_in_range as $occupant)
			{
				if ($occupant['start_date'] <= $i && ($occupant['end_date'] >= $i-50000 || !(int)$occupant['end_date']))
				{
					$return['days_per_resident'][$occupant['resident_id']]++;
					$occupants_for_day[] = $occupant;
				}
			}

			foreach($occupants_for_day as $occupant_current)
			{
				$resident_cost = $return['costs'][$occupant_current['id']];
				$amount = $resident_cost['cost'] / $num_days;
				
				$resident_id = $occupant_current['resident_id'];
				
				if (!isset($return['totals'][$resident_id]))
					$return['totals'][$resident_id] = 0;
				
				if ($return['billing_type'] == "apartment")
					$return['totals'][$resident_id] += ($amount / count($occupants_for_day));
				else
					$return['totals'][$resident_id] += $amount;
			}
		}
		
		// Clean up the resulting number.
		if ($return['totals'])
		{
			foreach($return['totals'] as $resident_id => $total)
			{
				$subtotal = round($total, 3);
				$subtotal = \DF\Utilities::ceiling($subtotal, 2);
				
				$return['totals'][$resident_id] = $subtotal;
			}
		}
		
		return $return;
	}
	
	/**
	 * Generate any applicable late fees.
	 */
	public static function generateLateFees($base_timestamp = NULL)
	{
		// Allow for calling this function with a custom timestamp.
		$base_timestamp = (is_null($base_timestamp)) ? time() : (int)$base_timestamp;
		
		$late_fee_threshold_days = (int)Settings::getSetting('late_fee_threshold', 0);
		$late_fee_amount = (float)Settings::getSetting('late_fee', 0);
		
		if ($late_fee_threshold_days != 0 && $late_fee_amount != 0)
		{
			// Pull all transactions that are unpaid and were posted prior to this date.
			$late_fee_threshold = $base_timestamp - ($late_fee_threshold_days * 86400);
			
			$late_records = Doctrine_Query::create()
				->from('Split s')->leftJoin('s.Transaction t')->leftJoin('s.Resident r')
				->addWhere('t.transaction_type = ?', RegisterTransaction::TRANSACTION_TYPE_RENT)
				->addWhere('s.matching_split_id = 0 OR s.matching_split_id IS NULL')
				->addWhere('s.due_date < ?', $late_fee_threshold)
				->addWhere('(r.exempt_from_late_fees IS NULL OR r.exempt_from_late_fees != ?)', 1)
				->execute(array(), Doctrine_Core::HYDRATE_ARRAY);
			
			foreach((array)$late_records as $late_split)
			{
				set_time_limit(30);
				
				// Normalize today's date.
				$today = mktime(12, 0, 0, date('d', $base_timestamp), date('m', $base_timestamp), date('Y', $base_timestamp));
				
				// Check for existing split with this information.
				$existing_fee = Doctrine_Query::create()
					->from('Split s')->leftJoin('s.Transaction t')
					->addWhere('t.transaction_type = ?', RegisterTransaction::TRANSACTION_TYPE_LATEFEE)
					->addWhere('s.resident_id = ?', $late_split['resident_id'])
					->addWhere('s.asset_id = ?', $late_split['asset_id'])
					->addWhere('s.due_date = ?', $today)
					->fetchOne();
				
				// If no existing split, create a new late fee.
				if (!$existing_fee)
				{
					$transaction = new RegisterTransaction();
					$transaction->posted = time();
					$transaction->transaction_type = RegisterTransaction::TRANSACTION_TYPE_LATEFEE;
					$transaction->date = time();
					$transaction->asset_id = $late_split['asset_id'];
					$transaction->memo = 'Late Fee for Rent Charges';
					$transaction->save();
					$transaction_id = $transaction->id;
					
					$split = new Split();
					$split->posted = time();
					$split->transaction_id = $transaction_id;
					$split->resident_id = $late_split['resident_id'];
					$split->ledger_id = Split::LEDGER_STANDARD;
					$split->credit_or_debit = 'D';
					$split->item_id = Item::fetchByName('Late Rent Penalties');
					$split->asset_id = $late_split['asset_id'];
					$split->split_amount = 0-$late_fee_amount;
					$split->due_date = $today;
					$split->save();
					
					Split::updateRunningBalance($split->resident_id, $split->ledger_id);
					
					// Send e-mail notification.
					DF_Messenger::send(array(
						'to'        => array($late_split['Resident']['email'], $late_split['Resident']['preferred_email']),
						'subject'   => 'Late Fee Posted',
						'template'  => 'billing/late',
						'vars'      => array(
							'amount' => $late_fee_amount,
						),
					));
				}
			}
		}
	}
	
	/**
	 * Generate recurring charges.
	 */
	
	public static function generateRecurringCharges()
	{
		$recurring_charges = Doctrine_Query::create()
			->from('Split s')->leftJoin('s.Transaction t')
			->where('s.recurring_type != ?', Split::RECURRING_INACTIVE)
			->addWhere('s.recurring_active = ?', 1)
			->fetchArray();
		
		foreach((array)$recurring_charges as $recurring_split)
		{
			// Check if the recurring charge is still active.
			$occupancies = Occupancy::getActiveByResidentId($recurring_split['resident_id']);
			
			if (empty($occupancies))
			{
				$split = Split::fetchById($recurring_split['id']);
				$split->recurring_active = 0;
				$split->save();
			}
			else
			{
				// Use due date if specified, otherwise use posted date.
				if ($recurring_split['due_date'] != 0)
					$start_date = $recurring_split['due_date'];
				else
					$start_date = $recurring_split['posted'];
				
				if ($start_date != 0)
				{
					$i = 1;
					$today = mktime(12, 0, 0);
					
					while($i <= 1460)
					{
						// Generate recurring date and check for validity.
						if ($recurring_split['recurring_type'] == Split::RECURRING_DAILY)
							$recurring_date = mktime(12, 0, 0, date('m', $start_date), date('d', $start_date)+$i, date('Y', $start_date));
						else if ($recurring_split['recurring_type'] == Split::RECURRING_MONTHLY)
							$recurring_date = mktime(12, 0, 0, date('m', $start_date)+$i, date('d', $start_date), date('Y', $start_date));
						
						if ($recurring_date > $today || !$recurring_date)
							break;
						
						// Check for existing charge posted on that date.
						$existing_charge = Doctrine_Query::create()
							->from('Split s')
							->where('s.resident_id = ?', $recurring_split['resident_id'])
							->addWhere('s.recurring_parent_id = ?', $recurring_split['id'])
							->addWhere('s.posted = ?', $recurring_date)
							->fetchOne();
						
						if (!$existing_charge)
						{
							// Create new charge.
							$split = new Split();
							
							$split->posted = $recurring_date;
							$split->due_date = $recurring_date;
							$split->transaction_id = (int)$recurring_split['transaction_id'];
							$split->resident_id = (int)$recurring_split['resident_id'];
							$split->ledger_id = (int)$recurring_split['ledger_id'];
							$split->credit_or_debit = 'D';
							$split->split_amount = $recurring_split['split_amount'];
							$split->recurring_parent_id = $recurring_split['id'];
							
							if ($recurring_split['item_id'])
								$split->item_id = $recurring_split['item_id'];
							
							if ($recurring_split['asset_id'])
								$split->asset_id = $recurring_split['asset_id'];
							
							$split->save();
							
							Split::updateRunningBalance($split->resident_id, $split->ledger_id);
						}
						
						$i++;
					}
				}
			}
		}
	}

	/**
	 * Logging
	 */
	static $log_contents;
	public static function writeLog($data)
	{
		self::$log_contents[] = $data;
	}
	public static function getLog()
	{
		return self::$log_contents;
	}
}