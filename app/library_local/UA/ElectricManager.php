<?php
/**
 * University Apartments electric manager class.
 */

class UA_ElectricManager
{
    /**
     * Generate electric charges for the current month.
     */
    public static function generateCharges($asset_id = NULL, $report_only = FALSE)
    {
        $report = array();
        $start_threshold = time()-(86400*60);
        
        $all_pending = Electric::fetchUnbilled($asset_id);
        $electric_item_id = Item::fetchByName('Electricity');
        
        if (!$all_pending)
            return;
        
        foreach($all_pending as $electric)
        {
            set_time_limit(60);

            $asset = $electric->Asset;
            $asset_name = $asset->name;

            $electric_id = $electric->id;
            $report[$electric_id] = array(
                'asset_name'    => $asset_name,
                'asset_location' => $asset->AssetLocation->name,
            );
            
            if ($asset->is_exempt_electrical == 1)
            {
                if (!$report_only)
                {
                    $electric->is_billed = 1;
                    $electric->save();
                }

                $report[$electric_id]['is_billed'] = FALSE;
                $report[$electric_id]['message'] = 'Exempt from electric billing, no charges posted.';
                continue;
            }
            
            $previous_reading = Electric::fetchPrevious($electric);
            
            if (!$previous_reading)
            {
                if (!$report_only)
                {
                    $electric->is_billed = 1;
                    $electric->save();
                }

                $report[$electric_id]['is_billed'] = FALSE;
                $report[$electric_id]['message'] = 'No previous reading could be located.';
                continue;
            }
            
            $b_timestamp = (int)$electric->meter_date;
            $a_timestamp = (int)$previous_reading->meter_date;
            $mid_timestamp = ($a_timestamp + $b_timestamp) / 2;

            $b_usage = $electric->meter_reading % 10000;
            $a_usage = $previous_reading->meter_reading % 10000;
            $total_usage = $b_usage - $a_usage;
            
            // Handle rollover past 10000, or error.
            if ($total_usage < 0 && $a_usage > 9000)
                $total_usage = (10000 - $a_usage) + $b_usage;
            
            // Append details to report.
            $report[$electric_id]['service_start'] = $a_timestamp;
            $report[$electric_id]['service_end'] = $b_timestamp;
            $report[$electric_id]['usage_start'] = $a_usage;
            $report[$electric_id]['usage_end'] = $b_usage;
            
            if ($total_usage < 0)
            {
                if (!$report_only)
                {
                    $electric->is_billed = 1;
                    $electric->save();
                }

                $report[$electric_id]['is_billed'] = FALSE;
                $report[$electric_id]['message'] = 'Newer Reading ('.$b_usage.') lower than older reading ('.$a_usage.')';
                continue;
            }
            
            $rate = $asset->ElectricProvider->cost;
            $cost = $total_usage * $rate;

            $report[$electric_id]['cost'] = '$'.number_format($cost, 2);
            
            // Get residents living in the apartment within the billable time.
            $occupancies_in_range = Occupancy::getActiveByAsset($asset->id, (int)$mid_timestamp);
            
            if (empty($occupancies_in_range))
            {
                if (!$report_only)
                {
                    $electric->is_billed = 1;
                    $electric->save();
                }

                $report[$electric_id]['is_billed'] = FALSE;
                $report[$electric_id]['message'] = 'Nobody living in apartment during range. Ignoring billing.';
                continue;
            }
            
            $num_occupants = count($occupancies_in_range);
            $cost_per_resident = \DF\Utilities::ceiling($cost / $num_occupants, 2);

            if ($cost_per_resident > 0)
            {
                if (!$report_only)
                {
                    // Post new transaction.
                    $transaction = new RegisterTransaction();
                    $transaction->posted = time();
                    $transaction->date = time();
                    $transaction->transaction_type = RegisterTransaction::TRANSACTION_TYPE_ELECTRIC;
                    $transaction->asset_id = $asset->id;
                    $transaction->memo = 'Electric Charges - '.$asset_name.' - Usage from '.date('m/d/Y', $a_timestamp).' to '.date('m/d/Y', $b_timestamp).' ('.$total_usage.' kWh)';
                    $transaction->save();
                    $transaction_id = $transaction->id;
                    
                    foreach($occupancies_in_range as $occupancy)
                    {
                        // Post new split for this resident.
                        $split = new Split();
                        $split->posted = time();
                        $split->transaction_id = $transaction_id;
                        $split->resident_id = $occupancy['Resident']['id'];
                        $split->ledger_id = Split::LEDGER_STANDARD;
                        $split->credit_or_debit = 'D';
                        $split->item_id = $electric_item_id;
                        $split->asset_id = $asset->id;
                        $split->due_date = time();
                        $split->split_amount = 0-$cost_per_resident;
                        $split->save();
                        
                        Split::updateRunningBalance($split->resident_id, $split->ledger_id);
                    }
                    
                    $electric->is_billed = 1;
                    $electric->save();
                }

                $report[$electric_id]['is_billed'] = TRUE;
            }
            else
            {
                if (!$report_only)
                {
                    $electric->is_billed = 1;
                    $electric->save();
                }

                $report[$electric_id]['is_billed'] = FALSE;
                $report[$electric_id]['message'] = 'Amount billed would be zero; no bill posted.';
            }
        }
        
        return $report;
    }
}