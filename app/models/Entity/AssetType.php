<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * AssetType
 *
 * @Table(name="asset_type")
 * @Entity
 */
class AssetType extends \DF\Doctrine\Entity
{
    const ADMINISTRATIVE                                             =  -1;
    const UNCLASSIFIED                                               =   0;
    const AA                                                         =   1;
    const CV                                                         =   2;
    const H                                                          =   3;
    const CA1                                                        =   4;
    const CA2                                                        =   5;
    const CAS                                                        =   6;
    const STANDARD_A                                                 =  61;
    const STANDARD_B                                                 =  62;
    const STANDARD_C                                                 =  63;
    const STANDARD_D                                                 =  64;
    const PREMIUM_A                                                  =  65;
    const PREMIUM_B                                                  =  66;
    const PREMIUM_C                                                  =  67;
    const PREMIUM_D                                                  =  68;
    const COLLEGE_AVENUE_APARTMENTS_TWO_BEDROOM_STUDIO_FURNISHED     =  69;
    const COLLEGE_AVENUE_APARTMENTS_TWO_BEDROOM_STUDIO_UNFURNISHED   =  70;
    const SU                                                         =  89;
    const SF                                                         =  90;
    const SCHOB_HOUSE                                                = 109;
    const ASSIGNABLE_STORAGE_CLOSET                                  = 129;

    public function __construct()
    {
        $this->costs = new ArrayCollection;
        $this->assets = new ArrayCollection;
    }

    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="name", type="string", length=64, nullable=true) */
    protected $name;

    /** @Column(name="description", type="string", length=256, nullable=true) */
    protected $description;

    /** @Column(name="asset_category_id", type="integer", nullable=true) */
    protected $asset_category_id;

    /** @Column(name="asset_waiting_list_category", type="integer", nullable=true) */
    protected $asset_waiting_list_category;

    /** @Column(name="occupancy_model_id", type="integer", nullable=true) */
    protected $occupancy_model_id;

    /** @Column(name="is_eligible_for_renewal", type="integer", nullable=true) */
    protected $is_eligible_for_renewal;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * @ManyToOne(targetEntity="OccupancyModel")
     * @JoinColumn(name="occupancy_model_id", referencedColumnName="id")
     */
    protected $occupancy_model;

    /** @OneToMany(targetEntity="AssetCost", mappedBy="asset_type") */
    protected $costs;

    /** @OneToMany(targetEntity="Asset", mappedBy="type") */
    protected $assets;

    public static function getAssetTypeCodes()
    {
        return array(
            self::ADMINISTRATIVE    => 'Administrative',
            self::UNCLASSIFIED      => 'Unclassified',
            self::AA                => 'Avenue A',
            self::CV                => 'College View',
            self::H                 => 'Hensel',
            self::CA1               => 'College Avenue 1',
            self::CA2               => 'College Avenue 2',
            self::CAS               => 'College Avenue 3',
            self::STANDARD_A        => 'Gardens Standard A',
            self::STANDARD_B        => 'Gardens Standard B',
            self::STANDARD_C        => 'Gardens Standard C',
            self::STANDARD_D        => 'Gardens Standard D',
            self::PREMIUM_A         => 'Gardens Premium A',
            self::PREMIUM_B         => 'Gardens Premium B',
            self::PREMIUM_C         => 'Gardens Premium C',
            self::PREMIUM_D         => 'Gardens Premium D',
            self::COLLEGE_AVENUE_APARTMENTS_TWO_BEDROOM_STUDIO_FURNISHED    => 'College Avenue Studio - Furnished',
            self::COLLEGE_AVENUE_APARTMENTS_TWO_BEDROOM_STUDIO_UNFURNISHED  => 'College Avenue Studio - Unfurnished',
            self::SU                => 'College Avenue Studio - Unfurnished',
            self::SF                => 'College Avenue Studio - Furnished',
            self::SCHOB_HOUSE       => 'Schob House',
            self::ASSIGNABLE_STORAGE_CLOSET    => 'Assignable Storage Closet',
        );
    }
    
    public static function getAssetTypeCode($code_id)
    {
        $asset_type_codes = self::getAssetTypeCodes();
        return $asset_type_codes[$code_id];
    }
    
    public function getAssetTypeText()
    {
        return self::getAssetTypeCode($this->asset_category_id);
    }

    public static function fetchById($id)
    {
        return self::find($id);
    }
    public static function fetchByName($name)
    {
        return self::getRepository()->findOneBy(array('name' => $name));
    }   
    
    public static function checkExists($name)
    {
        $obj = self::fetchByName($name);
        return is_a($obj, __CLASS__);
    }   
    
    public static function fetchApartmentTypes()
    {
        $em = self::getEntityManager();
        $all_records_raw = $em->createQuery('SELECT t, om FROM '.__CLASS__.' t LEFT JOIN t.occupancy_model om WHERE t.asset_waiting_list_category = :cat ORDER BY t.description ASC')
            ->setParameter('cat', 1)
            ->getArrayResult();

        $all_records = array();
        foreach($all_records_raw as $record)
        {
            $all_records[$record['id']] = $record;
        }
        
        return $all_records;
    }
}