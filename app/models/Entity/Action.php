<?php
namespace Entity;

/**
 * @Table(name="action")
 * @Entity
 */
class Action extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->roles = new ArrayCollection;
    }
    
    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /** @Column(name="name", type="string", length=100, nullable=true, nullable=true) */
    protected $name;
    
    /** @ManyToMany(targetEntity="Role", mappedBy="actions") */
    protected $roles;
}