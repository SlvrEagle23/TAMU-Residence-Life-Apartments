<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * GuestRoomReservation
 *
 * @Table(name="guest_room_reservation")
 * @Entity
 */
class GuestRoomReservation extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="sponsored_guest_id", type="integer", nullable=true) */
    protected $sponsored_guest_id;

    /** @Column(name="request_type_id", type="integer", nullable=true) */
    protected $request_type_id;

    /** @Column(name="check_in_date", type="integer", nullable=true) */
    protected $check_in_date;

    /** @Column(name="check_out_date", type="integer", nullable=true) */
    protected $check_out_date;

    /** @Column(name="department_id", type="integer", nullable=true) */
    protected $department_id;

    /** @Column(name="sponsor_id", type="integer", nullable=true) */
    protected $sponsor_id;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * @ManyToOne(targetEntity="GuestRoomRequestType")
     * @JoinColumn(name="request_type_id", referencedColumnName="id")
     */
    protected $type;

    /**
     * @ManyToOne(targetEntity="SponsoredGuest")
     * @JoinColumn(name="sponsored_guest_id", referencedColumnName="id")
     */
    protected $sponsored_guest;

    /**
     * @ManyToOne(targetEntity="Department")
     * @JoinColumn(name="department_id", referencedColumnName="id")
     */
    protected $department;

    /**
     * @ManyToOne(targetEntity="Sponsor")
     * @JoinColumn(name="sponsor_id", referencedColumnName="id")
     */
    protected $sponsor;
}