<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * Event
 *
 * @Table()
 * @Entity
 */
class Event extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="name", type="string", length=128, nullable=true) */
    protected $name;

    /** @Column(name="description", type="text", nullable=true) */
    protected $description;

    /** @Column(name="start_time", type="integer", nullable=true) */
    protected $start_time;

    /** @Column(name="end_date", type="integer", nullable=true) */
    protected $end_date;

    /** @Column(name="all_day", type="boolean", nullable=true) */
    protected $all_day;

    /** @Column(name="location", type="string", length=128, nullable=true) */
    protected $location;

    /** @Column(name="event_type_id", type="integer", nullable=true) */
    protected $event_type_id;

    /** @Column(name="user_id", type="integer", nullable=true) */
    protected $user_id;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * @ManyToOne(targetEntity="EventType")
     * @JoinColumn(name="event_type_id", referencedColumnName="id")
     */
    protected $type;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ManyToMany(targetEntity="User")
     * @JoinTable(name="event_has_attendees",
     *      joinColumns={@JoinColumn(name="event_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="user_id", referencedColumnName="id")}
     * )
     */
    protected $attendees;

}