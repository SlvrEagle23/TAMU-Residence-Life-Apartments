<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * Phone
 *
 * @Table()
 * @Entity
 */
class Phone extends \DF\Doctrine\Entity
{
    /**
     * @var decimal $id
     *
     * @Column(name="id", type="decimal", precision=18, scale=2, nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer $asset_id
     *
     * @Column(name="asset_id", type="integer", nullable=true)
     */
    protected $asset_id;

    /**
     * @var string $phone_number
     *
     * @Column(name="phone_number", type="string", length=16, nullable=true)
     */
    protected $phone_number;

    /**
     * @var string $modem_serial_number
     *
     * @Column(name="modem_serial_number", type="string", length=128, nullable=true)
     */
    protected $modem_serial_number;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * @ManyToOne(targetEntity="Asset")
     * @JoinColumn(name="asset_id", referencedColumnName="id")
     */
    protected $asset;
}