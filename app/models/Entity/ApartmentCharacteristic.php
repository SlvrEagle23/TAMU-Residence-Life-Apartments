<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * AparmtentCharacteristic
 *
 * @Table(name="apartment_characteristics")
 * @Entity
 */
class ApartmentCharacteristic extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="asset_id", type="integer", nullable=true) */
    protected $asset_id;

    /** @Column(name="floor", type="integer", nullable=true) */
    protected $floor;

    /** @Column(name="maximum_residents", type="integer", nullable=true) */
    protected $maximum_residents;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * @ManyToOne(targetEntity="Asset")
     * @JoinColumn(name="asset_id", referencedColumnName="id")
     */
    protected $asset;
}