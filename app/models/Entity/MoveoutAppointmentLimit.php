<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * MoveoutAppointmentLimit
 *
 * @Table(name="moveout_appointment_limit")
 * @Entity
 */
class MoveoutAppointmentLimit extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="date", type="integer", nullable=true) */
    protected $date;

    /** @Column(name="max_appointments", type="integer", nullable=true) */
    protected $max_appointments;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;
}