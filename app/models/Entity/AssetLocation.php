<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * AssetLocation
 *
 * @Table(name="asset_location")
 * @Entity
 */
class AssetLocation extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="name", type="string", length=64, nullable=true) */
    protected $name;

    /** @Column(name="support_account", type="integer", nullable=true) */
    protected $support_account;

    /** @Column(name="billing_item_id", type="integer", nullable=true) */
    protected $billing_item_id;

    /** @Column(name="address", type="text", nullable=true) */
    protected $address;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;
}