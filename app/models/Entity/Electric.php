<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * Electric
 *
 * @Table()
 * @Entity
 */
class Electric extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="asset_id", type="integer", nullable=true) */
    protected $asset_id;

    /** @Column(name="reading_type", type="string", length=50, nullable=true) */
    protected $reading_type;

    /** @Column(name="meter_reading", type="decimal", precision=18, scale=2, nullable=true) */
    protected $meter_reading;

    /** @Column(name="meter_date", type="integer", nullable=true) */
    protected $meter_date;

    /** @Column(name="meter_month", type="integer", nullable=true) */
    protected $meter_month;

    /** @Column(name="is_billed", type="boolean", nullable=true) */
    protected $is_billed;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * @ManyToOne(targetEntity="Asset")
     * @JoinColumn(name="asset_id", referencedColumnName="id")
     */
    protected $asset;
}