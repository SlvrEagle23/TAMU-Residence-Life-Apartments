<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * RegisterTransaction
 *
 * @Table(name="register_transaction")
 * @Entity
 */
class RegisterTransaction extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer $asset_id
     *
     * @Column(name="asset_id", type="integer", nullable=true)
     */
    protected $asset_id;

    /**
     * @var integer $workorder_id
     *
     * @Column(name="workorder_id", type="integer", nullable=true)
     */
    protected $workorder_id;

    /**
     * @var integer $resident_id
     *
     * @Column(name="resident_id", type="integer", nullable=true)
     */
    protected $resident_id;

    /**
     * @var integer $transaction_type
     *
     * @Column(name="transaction_type", type="integer", nullable=true)
     */
    protected $transaction_type;

    /**
     * @var integer $date
     *
     * @Column(name="date", type="integer", nullable=true)
     */
    protected $date;

    /**
     * @var string $memo
     *
     * @Column(name="memo", type="string", length=256, nullable=true)
     */
    protected $memo;

    /**
     * @var string $check_reference_number
     *
     * @Column(name="check_reference_number", type="string", length=32, nullable=true)
     */
    protected $check_reference_number;

    /**
     * @var string $no_sale
     *
     * @Column(name="no_sale", type="string", length=1, nullable=true)
     */
    protected $no_sale;

    /**
     * @var integer $posted
     *
     * @Column(name="posted", type="integer", nullable=true)
     */
    protected $posted;

    /**
     * @var integer $register_id
     *
     * @Column(name="register_id", type="integer", nullable=true)
     */
    protected $register_id;

    /**
     * @var string $reference_number
     *
     * @Column(name="reference_number", type="string", length=256, nullable=true)
     */
    protected $reference_number;

    /**
     * @var integer $transaction_source_id
     *
     * @Column(name="transaction_source_id", type="integer", nullable=true)
     */
    protected $transaction_source_id;

    /**
     * @var integer $user_id
     *
     * @Column(name="user_id", type="integer", nullable=true)
     */
    protected $user_id;

    /**
     * @var string $validate_print
     *
     * @Column(name="validate_print", type="string", length=1, nullable=true)
     */
    protected $validate_print;

    /**
     * @var integer $touchnet_id
     *
     * @Column(name="touchnet_id", type="integer", nullable=true)
     */
    protected $touchnet_id;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ManyToOne(targetEntity="Register")
     * @JoinColumn(name="register_id", referencedColumnName="id")
     */
    protected $register;

    /**
     * @ManyToOne(targetEntity="Asset")
     * @JoinColumn(name="asset_id", referencedColumnName="id")
     */
    protected $asset;

    /**
     * @ManyToOne(targetEntity="Resident")
     * @JoinColumn(name="resident_id", referencedColumnName="id")
     */
    protected $resident;

    /**
     * @ManyToOne(targetEntity="TransactionSource")
     * @JoinColumn(name="transaction_source_id", referencedColumnName="id")
     */
    protected $source;

    /**
     * @ManyToOne(targetEntity="Touchnet")
     * @JoinColumn(name="touchnet_id", referencedColumnName="id")
     */
    protected $touchnet;
}