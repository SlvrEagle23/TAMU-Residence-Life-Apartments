<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * ElectricHistory
 *
 * @Table(name="electric_history")
 * @Entity
 */
class ElectricHistory extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="asset_id", type="integer", nullable=true) */
    protected $asset_id;

    /** @Column(name="meter_month", type="integer", nullable=true) */
    protected $meter_month;

    /** @Column(name="meter_change", type="decimal", precision=18, scale=2, nullable=true) */
    protected $meter_change;

    /** @Column(name="rate", type="decimal", precision=18, scale=2, nullable=true) */
    protected $rate;

    /** @Column(name="cost", type="decimal", precision=18, scale=2, nullable=true) */
    protected $cost;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * @ManyToOne(targetEntity="Asset")
     * @JoinColumn(name="asset_id", referencedColumnName="id")
     */
    protected $asset;
}