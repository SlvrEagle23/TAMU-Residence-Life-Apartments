<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * Vehicle
 *
 * @Table()
 * @Entity
 */
class Vehicle extends \DF\Doctrine\Entity
{
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer $resident_id
     *
     * @Column(name="resident_id", type="integer", nullable=true)
     */
    protected $resident_id;

    /**
     * @var string $permit
     *
     * @Column(name="permit", type="string", length=16, nullable=true)
     */
    protected $permit;

    /**
     * @var string $license_plate
     *
     * @Column(name="license_plate", type="string", length=16, nullable=true)
     */
    protected $license_plate;

    /**
     * @var string $make
     *
     * @Column(name="make", type="string", length=24, nullable=true)
     */
    protected $make;

    /**
     * @var string $model
     *
     * @Column(name="model", type="string", length=24, nullable=true)
     */
    protected $model;

    /**
     * @var string $registration_state
     *
     * @Column(name="registration_state", type="string", length=24, nullable=true)
     */
    protected $registration_state;

    /**
     * @var integer $occupancy_id
     *
     * @Column(name="occupancy_id", type="integer", nullable=true)
     */
    protected $occupancy_id;

    /**
     * @var string $color
     *
     * @Column(name="color", type="string", length=24, nullable=true)
     */
    protected $color;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * @ManyToOne(targetEntity="Resident")
     * @JoinColumn(name="resident_id", referencedColumnName="id")
     */
    protected $resident;

    /**
     * @ManyToOne(targetEntity="Occupancy")
     * @JoinColumn(name="occupancy_id", referencedColumnName="id")
     */
    protected $occupancy;
}