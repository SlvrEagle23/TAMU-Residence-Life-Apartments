<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * ResidentType
 *
 * @Table(name="resident_type")
 * @Entity
 */
class ResidentType extends \DF\Doctrine\Entity
{
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $name
     *
     * @Column(name="name", type="string", length=24, nullable=true)
     */
    protected $name;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

}