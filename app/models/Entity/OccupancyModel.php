<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * OccupancyModel
 *
 * @Table(name="occupancy_model")
 * @Entity
 */
class OccupancyModel extends \DF\Doctrine\Entity
{
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $name
     *
     * @Column(name="name", type="string", length=32, nullable=true)
     */
    protected $name;

    /**
     * @var string $description
     *
     * @Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;


}