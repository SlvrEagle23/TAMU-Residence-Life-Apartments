<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * SplitLog
 *
 * @Table(name="split_log")
 * @Entity
 */
class SplitLog extends \DF\Doctrine\Entity
{
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer $split_id
     *
     * @Column(name="split_id", type="integer", nullable=true)
     */
    protected $split_id;

    /**
     * @var integer $user_id
     *
     * @Column(name="user_id", type="integer", nullable=true)
     */
    protected $user_id;

    /**
     * @var integer $timestamp
     *
     * @Column(name="timestamp", type="integer", nullable=true)
     */
    protected $timestamp;

    /**
     * @var string $changes
     *
     * @Column(name="changes", type="text", nullable=true)
     */
    protected $changes;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * @ManyToOne(targetEntity="Split")
     * @JoinColumn(name="split_id", referencedColumnName="id")
     */
    protected $split;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
}