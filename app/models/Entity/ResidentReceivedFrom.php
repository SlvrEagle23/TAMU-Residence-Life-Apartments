<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * ResidentReceivedFrom
 *
 * @Table(name="resident_received_from")
 * @Entity
 */
class ResidentReceivedFrom extends \DF\Doctrine\Entity
{
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer $resident_id
     *
     * @Column(name="resident_id", type="integer", nullable=true)
     */
    protected $resident_id;

    /**
     * @var string $received_from_name
     *
     * @Column(name="received_from_name", type="string", length=192, nullable=true)
     */
    protected $received_from_name;

    /**
     * @ManyToOne(targetEntity="Resident")
     * @JoinColumn(name="resident_id", referencedColumnName="id")
     */
    protected $resident;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;
}