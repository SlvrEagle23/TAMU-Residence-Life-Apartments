<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * Sponsor
 *
 * @Table()
 * @Entity
 */
class Sponsor extends \DF\Doctrine\Entity
{
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $name
     *
     * @Column(name="name", type="string", length=128, nullable=true)
     */
    protected $name;

    /**
     * @var string $account_number
     *
     * @Column(name="account_number", type="string", length=6, nullable=true)
     */
    protected $account_number;

    /**
     * @var integer $department_id
     *
     * @Column(name="department_id", type="integer", nullable=true)
     */
    protected $department_id;

    /**
     * @var string $telephone
     *
     * @Column(name="telephone", type="string", length=64, nullable=true)
     */
    protected $telephone;

    /**
     * @var string $email
     *
     * @Column(name="email", type="string", length=128, nullable=true)
     */
    protected $email;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * @ManyToOne(targetEntity="Department")
     * @JoinColumn(name="department_id", referencedColumnName="id")
     */
    protected $department;
}