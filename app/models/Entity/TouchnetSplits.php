<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * TouchnetSplits
 *
 * @Table(name="touchnet_splits")
 * @Entity
 */
class TouchnetSplits extends \DF\Doctrine\Entity
{
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer $touchnet_id
     *
     * @Column(name="touchnet_id", type="integer", nullable=true)
     */
    protected $touchnet_id;

    /**
     * @var integer $split_id
     *
     * @Column(name="split_id", type="integer", nullable=true)
     */
    protected $split_id;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;


}