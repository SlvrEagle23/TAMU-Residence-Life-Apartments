<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * ApplicationFee
 *
 * @Table(name="application_fee")
 * @Entity
 */
class ApplicationFee extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="application_id", type="integer", nullable=true) */
    protected $application_id;

    /** @Column(name="fee_amount", type="decimal", precision=18, scale=2, nullable=true) */
    protected $fee_amount;

    /** @Column(name="fee_date", type="integer", nullable=true) */
    protected $fee_date;

    /** @Column(name="fee_receipt", type="string", length=32, nullable=true) */
    protected $fee_receipt;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * @ManyToOne(targetEntity="Application")
     * @JoinColumn(name="application_id", referencedColumnName="id")
     */
    protected $application;
}