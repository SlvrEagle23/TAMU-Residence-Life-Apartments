<?php
namespace Entity;

use \Doctrine\Common\Collections\ArrayCollection;
use \Doctrine\Mapping as ORM;

/**
 * @Table(name="users")
 * @Entity
 */
class User extends \DF\Doctrine\Entity
{
	public function __construct()
    {
        $this->roles = new ArrayCollection;
        $this->residents = new ArrayCollection;

        $this->created_at = $this->updated_at = new \DateTime('NOW');
    }
    
    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="username", type="string", length=255, nullable=true) */
    protected $username;

    /** @Column(name="firstname", type="string", length=255, nullable=true) */
    protected $firstname;

    /** @Column(name="lastname", type="string", length=255, nullable=true) */
    protected $lastname;

    /** @Column(name="title", type="string", length=255, nullable=true) */
    protected $title;

    /** @Column(name="email", type="string", length=255, nullable=true) */
    protected $email;

    /** @Column(name="email2", type="string", length=255, nullable=true) */
    protected $email2;

    /** @Column(name="phone", type="string", length=20, nullable=true) */
    protected $phone;

    /** @Column(name="uin", type="string", length=20, nullable=true) */
    protected $uin;

    /** @Column(name="register_id", type="integer", nullable=true) */
    protected $register_id;

    /** @Column(name="active", type="boolean", nullable=true) */
    protected $active;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /** @Column(name="created_at", type="datetime", nullable=true) */
    protected $created_at;

    /** @Column(name="updated_at", type="datetime", nullable=true) */
    protected $updated_at;

	/**
     * @ManyToMany(targetEntity="Role", inversedBy="users")
     * @JoinTable(name="user_has_role",
     *      joinColumns={@JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="role_id", referencedColumnName="id")}
     * )
     */
    protected $roles;

    /** @OneToMany(targetEntity="Resident", mappedBy="user") */
    protected $residents;

    public function getResidentId()
    {
        if (count($this->residents) > 0)
            return $this->residents[0]->id;
        else
            return NULL;
    }
    public function getResident()
    {
        if (count($this->residents) > 0)
            return $this->residents[0];
        else
            return NULL;
    }

    /**
     * Static Functions
     */
    
    /* Get or create user by UIN. */
    public static function getOrCreate($uin)
    {
        $record = self::getRepository()->findOneBy(array('uin' => $uin));
        
        if (!($record instanceof self))
        {
            $record = new self();
            $record->username = $uin;
            $record->uin = $uin;
            $record->password = ' ';
            $record->assignFromDirectory();
            $record->save();
        }
        return $record;
    }
    
    public function assignFromDirectory()
    {
		$directory = \DF\Service\TamuRest::getByUin($this->uin);
		
		if ($directory)
		{
			// Special replacement rules for NetID.
			if (isset($directory['tamuEduPersonNetID'][0]))
                $this->username = $directory['tamuEduPersonNetID'][0];
			
			$replace_values = array(
				'firstname'		=> $directory['givenName'][0],
				'lastname'		=> $directory['sn'][0],
				'email'			=> $directory['mail'][0],
				'lphone'		=> preg_replace('/[^\d]/', '', $directory['telephoneNumber'][0]),
			);
			
			foreach($replace_values as $replace_local => $replace_value)
			{
				$replace_current = $this->{$replace_local};
				if (empty($replace_current) && !empty($replace_value))
					$this->{$replace_local} = $replace_value;
			}
		}
        
        return $this;
    }
}