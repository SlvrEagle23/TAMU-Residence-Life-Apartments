<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * MoveoutAppointment
 *
 * @Table(name="moveout_appointment")
 * @Entity
 */
class MoveoutAppointment extends \DF\Doctrine\Entity
{
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="resident_id", type="integer", nullable=true) */
    protected $resident_id;

    /** @Column(name="occupancy_id", type="integer", nullable=true) */
    protected $occupancy_id;

    /** @Column(name="date", type="integer", nullable=true) */
    protected $date;

    /** @Column(name="slot", type="string", length=16, nullable=true) */
    protected $slot;

    /** @Column(name="movein", type="boolean", nullable=true) */
    protected $movein;

    /** @Column(name="noshow_log", type="integer", nullable=true) */
    protected $noshow_log;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * @ManyToOne(targetEntity="Resident")
     * @JoinColumn(name="resident_id", referencedColumnName="id")
     */
    protected $resident;

    /**
     * @ManyToOne(targetEntity="Occupancy", inversedBy="moveout_appointments")
     * @JoinColumn(name="occupancy_id", referencedColumnName="id")
     */
    protected $occupancy;
}