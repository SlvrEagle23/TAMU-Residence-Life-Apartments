<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * AssetCost
 *
 * @Table(name="asset_cost")
 * @Entity
 */
class AssetCost extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="asset_id", type="integer", nullable=true) */
    protected $asset_id;

    /** @Column(name="asset_type_id", type="integer", nullable=true) */
    protected $asset_type_id;

    /** @Column(name="start_date", type="integer", nullable=true) */
    protected $start_date;

    /** @Column(name="end_date", type="integer", nullable=true) */
    protected $end_date;

    /** @Column(name="cost", type="decimal", precision=18, scale=2, nullable=true) */
    protected $cost;

    /** @Column(name="period", type="text", nullable=true) */
    protected $period;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * @ManyToOne(targetEntity="Asset")
     * @JoinColumn(name="asset_id", referencedColumnName="id")
     */
    protected $asset;

    /**
     * @ManyToOne(targetEntity="AssetType")
     * @JoinColumn(name="asset_type_id", referencedColumnName="id")
     */
    protected $asset_type;
}