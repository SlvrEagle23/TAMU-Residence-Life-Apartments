<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * LeaseVersion
 *
 * @Table(name="lease_version")
 * @Entity
 */
class LeaseVersion extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->created_at = $this->updated_at = new \DateTime('NOW');
    }

    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="type", type="string", length=50, nullable=true) */
    protected $type;

    /** @Column(name="path", type="string", length=255, nullable=true) */
    protected $path;

    /** @Column(name="created_at", type="datetime", nullable=true) */
    protected $created_at;

    /** @Column(name="updated_at", type="datetime", nullable=true) */
    protected $updated_at;

    const TYPE_MONTHTOMONTH = 1;
    const TYPE_FIXED_BYAPT = 2;
    const TYPE_FIXED_BYROOM = 3;

    public function getContent()
    {
        $template_path = Block::getTemplatePath($this->path);

        if (file_exists($template_path))
            return file_get_contents($template_path);
        else
            return '';
    }

    /** 
     * Static Functions
     */

    public static function buildFromOffer($offer)
    {
        $block_data = $offer->contract_data;

        $block_data['resident_name'] = $offer->resident->getNameLastFirst();
        $block_data['resident_uin'] = $offer->resident->uin;

        if ($block_data['is_vacant'] == 1)
            $block_data['apartment_name'] = $offer->asset->name;
        else
            $block_data['apartment_name'] = '';
        
        $block_data['apartment_address'] = $offer->asset->location->address;
        $block_data['rent_amount'] = '$'.number_format($offer->asset->getCost($offer->move_in_date), 2);

        $move_out_date = (int)$offer->move_out_date;
        $block_data['contract_start'] = date('F j, Y', $offer->move_in_date);
        $block_data['contract_end'] = ($move_out_date) ? date('F j, Y', $move_out_date) : 'N/A';

        $occupancy_model = $offer->asset->getOccupancyModel();
        if (!$occupancy_model)
            $occupancy_model = 'apartment';

        if ($move_out_date == 0)
            $lease_type = self::TYPE_MONTHTOMONTH;
        elseif ($occupancy_model == "apartment")
            $lease_type = self::TYPE_FIXED_BYAPT;
        else
            $lease_type = self::TYPE_FIXED_BYROOM;

        $lease = self::fetchLatest($lease_type);

        $block_vars = $block_data + array(
            'request'       => $_REQUEST,
            'get'           => $_GET,
            'post'          => $_POST,
            'server'        => $_SERVER,
        );

        return array(
            'lease'     => $lease,
            'html'      => self::contentReplace($lease->getContent(), $block_vars),
        );
    }
    
    public static function getTypes()
    {
        return array(
            self::TYPE_MONTHTOMONTH     => 'Month-To-Month',
            self::TYPE_FIXED_BYAPT      => 'Fixed-Length by Apartment',
            self::TYPE_FIXED_BYROOM     => 'Fixed-Length by Bedroom',
        );
    }

    public static function update($type, $html, $is_new_revision = FALSE)
    {
        if (!$is_new_revision)
            $record = self::fetchLatest($type);
        
        if (!($record instanceof LeaseVersion))
        {
            $record = new LeaseVersion;
            $record->type = $type;
            $record->path = 'lease_'.date('Ymd_his');
            $record->save();
        }

        $template_path = Block::getTemplatePath($record->path);
        @file_put_contents($template_path, $html);

        return $record;
    }
    
    public static function fetchLatest($type = self::TYPE_MONTHTOMONTH)
    {
        $em = self::getEntityManager();
        return $em->createQuery('SELECT lv FROM '.__CLASS__.' lv WHERE lv.type = :type ORDER BY lv.id DESC')
            ->setParameter('type', $type)
            ->setMaxResults(1)
            ->getOneOrNullResult();
    }
    public static function fetchById($id)
    {
        return self::find($id);
    }
    
    public static function contentReplace($content, $vars, $base='')
    {
        foreach((array)$vars as $var_key => $var_value)
        {
            if (is_array($var_value))
            {
                $content = self::contentReplace($content, $var_value, $var_key.'.');
            }
            else if ($var_value instanceof \Zend_Config)
            {
                $var_value = $var_value->toArray();
                $content = self::contentReplace($content, $var_value, $var_key.'.');
            }
            else if (!is_object($var_value))
            {
                $replace_key = '#'.$base.$var_key.'#';
                $content = str_replace($replace_key, $var_value, $content);
            }
        }
        
        return $content;
    }
}