<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * MaintenanceWorker
 *
 * @Table(name="maintenance_worker")
 * @Entity
 */
class MaintenanceWorker extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="name", type="string", length=128, nullable=true) */
    protected $name;

    /** @Column(name="active", type="boolean", nullable=true) */
    protected $active;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;
}