<?php
namespace Entity;
use \Doctrine\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * RoommateRequest
 *
 * @Table(name="roommate_request")
 * @Entity
 */
class RoommateRequest extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="request_received", type="integer", nullable=true) */
    protected $request_received;

    /** @Column(name="requesting_resident_id", type="integer", nullable=true) */
    protected $requesting_resident_id;

    /** @Column(name="asset_id", type="integer", nullable=true) */
    protected $asset_id;

    /** @Column(name="new_roommate_email", type="string", length=255, nullable=true) */
    protected $new_roommate_email;

    /** @Column(name="token", type="string", length=255, nullable=true) */
    protected $token;

    /** @Column(name="approvals", type="text", nullable=true) */
    protected $approvals;

    /** @Column(name="approved", type="integer", nullable=true) */
    protected $approved;

    /** @Column(name="application_id", type="integer", nullable=true) */
    protected $application_id;

    /** @Column(name="resident_id", type="integer", nullable=true) */
    protected $resident_id;

    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * @ManyToOne(targetEntity="Resident")
     * @JoinColumn(name="requesting_resident_id", referencedColumnName="id")
     */
    protected $requester;

    /**
     * @ManyToOne(targetEntity="Asset")
     * @JoinColumn(name="asset_id", referencedColumnName="id")
     */
    protected $asset;

    /**
     * @ManyToOne(targetEntity="Application")
     * @JoinColumn(name="application_id", referencedColumnName="id")
     */
    protected $application;

    /**
     * @ManyToOne(targetEntity="Resident")
     * @JoinColumn(name="resident_id", referencedColumnName="id")
     */
    protected $resident;

    public function getApprovals($resident_id = null)
    {
        $approvals = json_decode($this->approvals, true);
        if ($resident_id && $approvals)
        {
            return $approvals[(string)$resident_id];
        }
        else
        {
            return $approvals;
        }
    }

    public function setApprovals($approvals)
    {
        $this->approvals = json_encode($approvals);
    }

    public function isApproved()
    {
        // timestamp in 'approved' column = true
        if (trim($this->approved) != '')
            return true;

        // if any roommates' approvals contain 'false' = false
        $approvals = $this->getApprovals();
        foreach($approvals as $key => $value)
        {
            if (($value === false) || ($value === 'pending'))
                return false;
        }
        // otherwise
        return true;
    }

    public function sendInvite()
    {
        $activation_link = DF_Url::domain(true).DF_Url::route(array(
            'module' => 'apply',
            'controller' => 'roommates',
            'action' => 'accept',
            'id' => $this->id,
            'email' => $this->new_roommate_email,
            'token' => $this->token,
        ));

        $vars = array(
            'requester_name' => $this->Requester->getNameFirstLast(),
            'asset_name' => $this->Asset->name,
            'activation_link' => $activation_link,
        );
        DF_Messenger::send(array(
            'to' => $this->new_roommate_email,
            'subject' => 'University Apartments roommate request',
            'template' => 'apply/invite',
            'vars' => $vars,
        ));
    }

    /**
     * Static Functions
     */

    public static function fetchById($id)
    {
        return self::find($id);
    }

    public static function fetchByAssetId($asset_id)
    {
        return self::getRepository()->findBy(array('asset_id' => $asset_id));
    }

    public static function fetchByResidentId($id)
    {
        return self::getRepository()->findOneBy(array('resident_id' => $id));
    }

    public static function fetchByApplicationId($id)
    {
        return self::getRepository()->findOneBy(array('application_id' => $id));
    }

    
}