<?php
/*
 * Add/Edit Vehicle
 *
 */
$config = Zend_Registry::getInstance()->get('config');

return array(
    'method' => 'post',
    'elements' => array(
        'make' => array('text', array(
            'label' => 'Make',
            'required' => true,
        )),
        
        'model' => array('text', array(
            'label' => 'Model',
            'required' => true,
        )),
        
        'color' => array('text', array(
            'label' => 'Color',
            'required' => true,
        )),
        
        'license_plate' => array('text', array(
            'label' => 'License Plate',
            'required' => true,
        )),
        
        'registration_state' => array('select', array(
            'label' => 'Registration State',
            'multiOptions' => $config->general->states,
            'required' => true,
        )),
        
        'permit' => array('text', array(
            'label' => 'Permit',
            'required' => true,
        )),
        
        'submit' => array('submit', array(
            'label' => 'Save',
            'class' => 'ui-button',
        )),
    ),
);