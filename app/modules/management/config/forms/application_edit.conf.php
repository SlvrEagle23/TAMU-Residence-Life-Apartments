<?php
/**
 * Add Resident Type
 */

$form = array(	
    /**
     * Form Configuration
     */

    'method' => 'post',
    'groups' => array(
        'application_details' => array(
            'legend' => 'Application Details',
            'elements' => array(

                'application_date' => array('unixdatetime', array(
                    'label' => 'Application Date',
                    'required' => true,
                )),
                
                'desired_move_in' => array('unixdate', array(
                    'label' => 'Desired Move-in Date',
                    'required' => true,
                )),
                
                'requested_lease_length' => array('radio', array(
                    'label' => 'Desired Lease Length',
                    'multiOptions' => array(
                        '12' => '12-month lease',
                        '9' => '9-month lease',
                        '1' => 'month-to-month lease',
                    ),
                    'description' => 'Month-to-month leases are only available in the Avenue A, College View and Hensel Apartments',
                )),
        
                'floor_preference' => array('select', array(
                    'label' => 'Floor Preference',
                    'multiOptions' => array(
                        'N' => '',
                        '1' => 'Ground Floor',
                        '2' => 'Second Floor',
                        '3' => 'Third Floor (Gardens only)',
                    ),
                )),
        
                'special_needs' => array('textarea', array(
                    'label' => 'Other Special Needs/Roommate Requests',
                    'description' => 'A roommate request is not a guarantee.  We will make every effort to accommodate roommate requests based on space availability.',
                    'attribs' => array(
                        'style' => 'height:7em;width:95%;',
                    ),
                )),
        
                'military_vet' => array('radio', array(
                    'label' => 'Are you a military veteran?',
                    'multiOptions' => array(0 => 'No', 1 => 'Yes'),
                    'value' => 0,
                    'description' => 'Must provide documentation',
                )),
        
                'police_fireprotection' => array('radio', array(
                    'label' => 'Are you a current or former member of police/fire protection?',
                    'multiOptions' => array(0 => 'No', 1 => 'Yes'),
                    'value' => 0,
                    'description' => 'Must provide documentation',
                )),
                
                'application_status_id' => array('select', array(
                    'label' => 'Application Status',
                    'multiOptions' => \Entity\WaitingListStatus::fetchSelect(),
                )),

                'response_deadline' => array('unixdate', array(
                    'label' => 'Response Deadline',
                )),
            ),
        ),
        'apartment_types' => array(
            'legend' => 'Apartment Styles',
            'description' => 'Select your first, second, and subsequent choices below:',
            'elements' => array(),
        ),
        'submit' => array(
            'legend' => '',
            'elements' => array(
                'submit' => array('submit', array(
                    'type'	=> 'submit',
                    'label'	=> 'Save Changes',
                    'helper' => 'formButton',
                    'class' => 'ui-button',
                )),
            ),
        ),
    ),
);

$apt_styles = array();
foreach(\Entity\AssetType::fetchApartmentTypes() as $id => $type)
{
    $occupancy_model = (!empty($type['OccupancyModel']['name'])) ? $type['OccupancyModel']['name'] : 'other';
    $apt_styles[$occupancy_model][$id] = $type;
}
ksort($apt_styles);
foreach($apt_styles as $style => $types)
{
    if ($style != 'other')
        $header_txt = 'Available by the '.ucwords($style);
    else
        $header_txt = 'Other';
    
    $form['groups']['apartment_types']['elements']['header_'.$style] = array('markup', array(
        'attribs' => array(
            'markup' => "<strong>$header_txt</strong><br/>",
        ),
        'decorators' => array(
            'ViewHelper',
            array(array('data' => 'HtmlTag'), array('tag' => 'span', 'class' => 'element')),
            array('Label', array('tag' => 'span')),
            array(array('row' => 'HtmlTag'), array('tag' => 'div')),
        ),
    ));
    
    foreach($types as $id => $type)
    {
        $form['groups']['apartment_types']['elements']['type_'.$id] = array('select', array(
            'label' => $type['description'],
            'multiOptions' => array(
                '0' => '',
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7',
            ),
            'decorators' => array(
                'ViewHelper',
                array(array('data' => 'HtmlTag'), array('tag' => 'span', 'class' => 'element')),
                array('Label', array('tag' => 'span')),
                array(array('row' => 'HtmlTag'), array('tag' => 'div')),
            ),
        ));
    }
}

return $form;