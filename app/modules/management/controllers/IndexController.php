<?php
class Management_IndexController extends \DF\Controller\Action
{
    public function indexAction()
    {
		$this->redirectToRoute(array('module' => 'default', 'controller' => 'index', 'action' => 'index'));
	}
}