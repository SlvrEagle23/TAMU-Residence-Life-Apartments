<?php
class Management_ReportsController extends \DF\Controller\Action
{
    public function permissions()
    {
        return \DF\Acl::getInstance()->isAllowed('view residents');
    }

    public function indexAction()
    {}

    public function occupancyAction()
    {
        $ethnicity_codes = array(
            'O' => 'Asian',
            'N' => 'Native Hawaiian/Pacific Islander',
            'X' => 'Other',
            'T' => 'Two or more',
            'H' => 'Hispanic',
            'B' => 'Black',
            'W' => 'White',
        );
        
        if ($this->_hasParam('vacancy'))
        {
            set_time_limit(600);
            
            // Pull list of complexes.
            $complexes_raw = Doctrine_Query::create()
                ->from('Asset a')
                ->innerJoin('a.AssetType at')
                ->where('at.name = ?', 'Complex')
                ->fetchArray();
            
            $complexes = array();
            
            foreach((array)$complexes_raw as $complex)
            {
                if ($complex['name'] == 'Gardens Phase II')
                    continue; // don't include Gardens Phase II
                
                $complexes[$complex['id']] = array(
                    'name'      => $complex['name'],
                    'occupied'  => 0,
                    'vacant'    => 0,
                    'total'     => 0,
                    'buildings' => array(),
                );
            }
            
            // Pull list of buildings and their parent associations.
            $buildings_raw = Doctrine_Query::create()
                ->from('Asset a')
                ->innerJoin('a.AssetType at')
                ->where('at.name = ?', 'Building')
                ->fetchArray();
            $buildings = array();
            $building_parents = array();
            
            foreach((array)$buildings_raw as $building)
            {
                if (!in_array($building['asset_parent_id'], array_keys($complexes)))
                    continue; // don't include Gardens Phase II
                
                $buildings[$building['id']] = $building['name'];
                $complex_id = (int)$building['asset_parent_id'];
                
                $complexes[$complex_id]['buildings'][$building['id']] = array(
                    'name'      => $building['name'],
                    'occupied'  => 0,
                    'vacant'    => 0,
                    'total'     => 0,
                );
                
                $building_parents[$building['id']] = $complex_id;
            }
            
            // Pull all assets.
            $all_assets = Asset::fetchAllRentable();
            
            $totals_by_building = array();
            $totals_by_complex = array();
            
            $grand_totals = array();
            $all_occupants = array();
			
            $occupants_by_ethnicity = array();
            $total_occupants = 0;
			
            foreach($all_assets as $asset)
            {
                if ($asset->assetType->name == 'GP2') // don't include Gardens Phase II
                    continue;
                
                if (array_key_exists($asset['id'], $buildings))
                    $building_id = (int)$asset['id'];
                else
                    $building_id = (int)$asset['asset_parent_id'];
                
                $complex_id = (int)$building_parents[$building_id];
                
                if ($complex_id == 0 || $building_id == 0)
                {
                    $complex_id = 0;
                    $building_id = $asset['id'];
                    $complexes[0]['buildings'][$building_id]['name'] = $asset['name'];
                }
                
                $asset_type = $asset->assetType->description;
                
                $complexes[$complex_id]['total']++;
                $complexes[$complex_id]['buildings'][$building_id]['total']++;
                $grand_totals['total']++;
                
                $occupants = $asset->getPrincipalResidents();
                
                if (count($occupants) > 0)
                {
                    $complexes[$complex_id]['occupied']++;
                    $complexes[$complex_id]['buildings'][$building_id]['occupied']++;
                    $grand_totals['occupied']++;
                    
                    foreach($occupants as $occupant)
                    {
                        if ($occupant->principal_resident)
                        {
                            if ($occupant->marital_status == 'M')
                                $status = 'Married';
                            else if ($occupant->marital_status == 'P')
                                $status = 'Single-Parent';
                            else
                                $status = 'Single';
                            
                            $classification = trim($occupant->classification);
                            if ($classification == 'P')
                                $all_occupants[$status . ' Post-Doctorates']++;
                            else if ((strpos($classification, 'G') !== false) || (strpos($classification, 'V') !== false) || (strpos($classification, 'M') !== false))
                                $all_occupants[$status . ' Graduate Students']++;
                            else
                                $all_occupants[$status . ' Undergraduate Students']++;
							
							$ethnicity = $ethnicity_codes[trim($occupant->ethnicity)];
							if ($ethnicity)
								$occupants_by_ethnicity[$ethnicity]++;
							else
								$occupants_by_ethnicity['None']++;
                            
                            $total_occupants++;
                        }
                    }
                }
                else
                {
                    $complexes[$complex_id]['vacant']++;
                    $complexes[$complex_id]['buildings'][$building_id]['vacant']++;
                    $grand_totals['vacant']++;
                }
            }
			
			ksort($all_occupants);
            ksort($assets_by_type);
			
			$this->view->assign(array(
                'complexes'         => $complexes,
				'grand_totals'		=> $grand_totals,
				'all_occupants'		=> $all_occupants,
				'total_occupants'	=> $total_occupants,
				'occupants_by_ethnicity' => $occupants_by_ethnicity,
				
				'executed'			=> TRUE,
			));
        }
        else
        {
            $this->view->executed = false;
        }
    }
    
    public function assetcostsAction()
    {
        $all_assets = Asset::fetchAll();
        
        $report = array();
        
        foreach($all_assets as $asset)
        {
            $asset_info = $asset->toArray();
            $asset_info['cost'] = $asset->getCost();
            
            $report[] = $asset_info;
        }
        
        $this->view->report = $report;
    }
	
    public function phoneAction()
    {
        $all_assets = Asset::fetchAll();
        $report = array();
        
        foreach($all_assets as $asset)
        {
            $asset_info = $asset->toArray();
            
            $occupants_raw = $asset->getPrincipalResidents();
            $occupants = array();
            if ($occupants_raw)
            {
                foreach($occupants_raw as $occupant)
                {
                    $occupants[] = $occupant->toArray();
                }
            }
            
            $asset_info['occupants'] = $occupants;
            
            $report[] = $asset_info;
        }
        
        $this->view->report = $report;
    }

    public function rosterAction()
    {
        set_time_limit(600);
        
        if ($this->_hasParam('list'))
        {
            $list = $this->_getParam('list');
            $this->view->list = $list;

            $all_assets = Asset::fetchAllRentable();
            $residents = array();
            
            foreach($all_assets as $asset)
            {
                $occupants = $asset->getPrincipalResidents();
                
                if (count($occupants) > 0)
                {
                    foreach($occupants as $occupant)
                    {
                        $family_raw = $occupant->getFamilyMembers();
                        $family = array();
                        foreach($family_raw as $family_item)
                        {
                            $family[] = $family_item->getNameFirstLast().' ('.$family_item->type->name.')';
                        }

                        if ($occupant['principal_resident'])
                        {
                            if (($list == "gp2" && $occupant->isExternal()) || ($list != "gp2" && !$occupant->isExternal()))
                            {
                                $occupant_array = $occupant->toArray();
                                $occupant_info = array(
                                    'id' => $occupant_array['id'],
                                    'first_name' => $occupant_array['first_name'],
                                    'middle_name' => $occupant_array['middle_name'],
                                    'surname' => $occupant_array['surname'],
                                    'uin' => $occupant_array['uin'],
                                    'email' => $occupant_array['email'],
                                    'phone' => $occupant_array['phone'],
                                    'family' => $family,
                                    'apt' => $asset->name,
                                );

                                $residents[] = $occupant_info;
                            }
                        }
                    }
                }
            }
            
            switch(strtolower($this->_getParam('format', 'html')))
            {
                case "csv":
                    $this->doNotRender();
                    DF_Export::exportToCSV($residents, TRUE);
                break;
                
                case "html":
                default:
                    $this->view->residents = $residents;
                    $this->view->executed = true;
                break;
            }
        }
        else
        {
            $this->view->executed = false;
        }
    }
}