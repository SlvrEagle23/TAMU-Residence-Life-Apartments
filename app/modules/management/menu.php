<?php
return array(
    'default' => array(
        'management'	=> array(
            // Home page.
            'label'		=> 'Manage',
            'module'	=> 'management',
            'permission' => 'view administration items',
            
            'pages'		=> array(
                'apartments'        => array(
                    'label' => 'Apartments',
                    'module' => 'management',
                    'controller' => 'apartments',
                    'action' => 'index',
                    'permission' => 'manage apartments',
                    'pages' => array(
                        'apartments_view' => array(
                            'module' => 'management',
                            'controller' => 'apartments',
                            'action' => 'view',
                        ),
                        'apartments_edit' => array(
                            'module' => 'management',
                            'controller' => 'apartments',
                            'action' => 'edit',
                        ),
                    )
                ),
                'applications'		=> array(
                    'label'	=> 'Applications',
                    'module' => 'management',
                    'controller' => 'applications',
                    'action' => 'index',
                    'permission' 	=> 'manage applications',
                    'pages'	=> array(),
                ),
                'residents'		=> array(
                    'label' 		=> 'Residents',
                    'module' 		=> 'management',
                    'controller' 	=> 'residents',
                    'action'            => 'index',
                    'permission' 	=> 'manage residents',
                    'pages'			=> array(),
                ),
            ),
        ),
    ),
);