<?php
/*
 * Add/Edit Quiz Result
 *
 */
$config = Zend_Registry::getInstance()->get('config');

return array(
    'method' => 'post',
    'elements' => array(
        'quiz_id' => array('select', array(
            'label' => 'Quiz',
            'required' => true,
            'multiOptions' => Quiz::fetchSelect(),
        )),
        
        'timestamp' => array('unixdate', array(
            'label' => 'Date Passed',
            'required' => true,
        )),
        
        'occupancy_id' => array('hidden', array(
            'label' => '',
        )),

        'resident_id' => array('hidden', array(
            'label' => '',
        )),
        
        'submit' => array('submit', array(
            'label' => 'Save',
            'class' => 'ui-button',
        )),
    ),
);