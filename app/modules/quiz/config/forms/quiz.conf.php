<?php
/**
 * Quiz form definition
 */

return array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'elements'		=> array(
            
			'name' => array('text', array(
				'label' => 'Quiz Name',
				'required' => true,
                'class' => 'half-width',
	        )),
            
            'description' => array('textarea', array(
               'label'  => 'Quiz Description',
               'required' => false,
               'class'  => 'half-width full-height',
            )),
			
			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Save Changes',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
);