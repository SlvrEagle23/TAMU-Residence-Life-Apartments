<?php
use \Entity\Quiz;
use \Entity\QuizQuestion;
use \Entity\QuizOption;

class Quiz_ManageController extends \DF\Controller\Action
{
    public function permissions()
    {
        return $this->acl->isAllowed('manage quizzes');
    }
    
    public function indexAction()
    {
        $this->view->all_quizzes = Quiz::fetchAll();
    }
    
    public function editquizAction()
    {
        $form = new \DF\Form($this->current_module_config->forms->quiz->form);
        
        $quiz_id = intval($this->_getParam('id'));
        
        if ($quiz_id != 0)
        {
            $record = Quiz::find($quiz_id);
            $form->setDefaults($record->toArray());
        }
		
		if (!empty($_POST) && $form->isValid($_POST))
		{
			$data = $form->getValues();
            
            if (!$record)
                $record = new Quiz();
            
            $record->fromArray($data);
			$record->save();
			
			$this->alert('Quiz updated!', 'green');
			$this->redirectFromHere(array('action' => 'index', 'id' => NULL));
            return;
		}

        $this->view->headTitle('Add/Edit Quiz');
        $this->renderForm($form);
    }
    
    public function deletequizAction()
    {
        $this->validateToken($this->_getParam('csrf'));
        
        $record = Quiz::find($this->_getParam('id'));
        if ($record)
            $record->delete();
        
        $this->alert('Record deleted!');
        $this->redirectFromHere(array('action' => 'index', 'id' => NULL, 'csrf' => NULL));
    }
    
    /**
     * View page for a specific quiz.
     */
    public function quizAction()
    {
        $quiz_id = $this->_getParam('id');
        $quiz = Quiz::find($quiz_id);
        
        if (!$quiz)
            throw new \DF\Exception\Warning('Quiz #'.$quiz_id.' not found!');
        
        $this->view->quiz = $quiz;
    }
    
    public function editquestionAction()
    {
        $quiz_id = $this->_getParam('id');
        $question_id = $this->_getParam('qid');
        
        $form = new \DF\Form($this->current_module_config->forms->quiz_question->form);
        
        if ($question_id != 0)
        {
            $record = QuizQuestion::find($question_id);
            
            $form_defaults = array(
                'title' => $record->question,
            );
            
            if (count($record->options) > 0)
            {
                $option_ids = array();
                $i = 1;
                foreach($record->options as $option)
                {
                    $form_defaults['option_'.$i] = $option->option_text;
                    $option_ids[$option->id] = $i;
                    $i++;
                }
            }
            
            if ($record->correct_answer_id)
            {
                $form_defaults['correct_answer'] = $option_ids[$record->correct_answer_id];
            }
            
            $form->setDefaults($form_defaults);
        }
        
        if (!empty($_POST) && $form->isValid($_POST))
        {
            $data = $form->getValues();
            
            if (!$record)
                $record = new QuizQuestion();
            
            $record->quiz_id = $quiz_id;
            $record->question = $data['title'];
            $record->correct_answer_id = NULL;
            $this->em->persist($record);
            
            // Delete existing question options.
            if (count($record->options) > 0)
            {
                foreach($record->options as $option)
                    $this->em->remove($option);
            }

            $option_ids = array();
            for($i = 1; $i <= $this->current_module_config->forms->quiz_question->num_options; $i++)
            {
                if (!empty($data['option_'.$i]))
                {
                    $option = new QuizOption;
                    $option->question = $record;
                    $option->option_text = $data['option_'.$i];
                    $this->em->persist($option);

                    $option_ids[$i] = $option;
                }
            }
            
            if ($data['correct_answer'] && isset($option_ids[$data['correct_answer']]))
            {
                $answer_id = $option_ids[$data['correct_answer']];
                $record->correct_answer = $answer_id;
                $this->em->persist($record);
            }

            $this->em->flush();
            
            $this->alert('Record updated!');
            $this->redirectFromHere(array('action' => 'quiz', 'id' => $quiz_id, 'qid' => NULL));
            return;
        }

        $this->view->headTitle('Add/Edit Quiz Question');
        $this->renderForm($form);
    }
    
    public function deletequestionAction()
    {
        $this->validateToken($this->_getParam('csrf'));
        
        $record = QuizQuestion::find($this->_getParam('qid'));
        if ($record)
        {
            if ($record->Options->count() > 0)
            {
                foreach($record->Options as $option)
                {
                    $option->delete();
                }
            }
            
            $record->delete();
        }
        
        $this->alert('Record deleted!');
        $this->redirectFromHere(array('action' => 'quiz', 'id' => $this->_getParam('id'), 'qid' => NULL, 'csrf' => NULL));
    }
}