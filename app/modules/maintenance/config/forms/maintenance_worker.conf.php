<?php
/**
 * Maintenance worker form.
 */

return array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'elements'		=> array(
            
            'name'	=> array('text', array(
				'label' => 'Worker Name',
				'required' => true,
                'class' => 'full-width',
			)),
            
			'active'	=> array('select', array(
				'label' => 'Is Active',
				'required' => true,
                'multiOptions' => array(1 => 'Yes', 0 => 'No'),
	        )),
            
			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Save Changes',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
);