<?php
class Maintenance_ManageController extends \DF\Controller\Action
{
	public function permissions()
	{
		return \DF\Acl::getInstance()->isAllowed('manage work orders');
	}
	
    public function indexAction()
    {
		$status_codes = MaintenanceWorkorder::getStatusCodes();
		$totals_by_status = MaintenanceWorkorder::fetchTotalsByStatus();
		$requests_by_status = array();
		
		foreach($status_codes as $code_id => $code_name)
		{
			$requests_by_status[$code_id] = array(
				'name'		=> $code_name,
				'count'		=> intval($totals_by_status[$code_id]),
			);
		}
		
		$this->view->requests_by_status = $requests_by_status;
	}
	
	public function requestsAction()
	{
		$query = Doctrine_Query::create()->from('MaintenanceWorkorder mw');
        
        $sortby = ($this->_getParam('sortby')) ? $this->_getParam('sortby') : 'id';
        $sortdir = ($this->_getParam('sortdir')) ? $this->_getParam('sortdir') : 'desc';
        
        switch($sortby)
        {
            case 'id':
                $query->addOrderBy('mw.id '.$sortdir);
            break;
            
            case 'category':
                $query->leftJoin('mw.Category c')->addOrderBy('c.name '.$sortdir);
            break;
            
            case 'status':
                $query->addOrderBy('mw.status '.$sortdir);
            break;
            
            case 'location':
                $query->leftJoin('mw.Asset a')->addOrderBy('a.name '.$sortdir);
            break;
            
            case 'time_reported':
                $query->addOrderBy('mw.time_reported '.$sortdir);
            break;
            
            case 'time_started':
                $query->addOrderBy('mw.time_started '.$sortdir);
            break;
            
            case 'time_closed':
                $query->addOrderBy('mw.time_closed '.$sortdir);
            break;
        }
        
		// Filter by status.
		if ($this->_hasParam('status'))
		{
			$status = $this->_getParam('status');
			
			if ($status == "active")
			{
				$active_status_codes = array(
					//MaintenanceWorkorder::STATUS_UNPROCESSED,
					MaintenanceWorkorder::STATUS_ACCEPTED,
					MaintenanceWorkorder::STATUS_ASSIGNED,
				);
				$query->addWhere('mw.status IN ?', array($active_status_codes));
			}
			else
			{
				$query->addWhere('mw.status = ?', $this->_getParam('status'));
			}
		}
		
		// Filter by Asset ID.
		if ($this->_hasParam('asset_id'))
		{
			$query->addWhere('asset_id = ?', $this->_getParam('asset_id'));
		}
		
		// Filter by Worker ID.
		if ($this->_hasParam('worker_id'))
		{
			$query->addWhere('mw.Tasks.Workers.id = ?', $this->_getParam('worker_id'));
		}
		
		// Filter by User ID.
		if ($this->_hasParam('user_id'))
		{
			$query->addWhere('mw.user_id = ?', $this->_getParam('user_id'));
		}
        
        // Filter by Work Order ID.
		if ($this->_hasParam('id'))
		{
			$query->addWhere('mw.id = ?', $this->_getParam('id'));
		}
        
        $page = ($this->_getParam('page')) ? $this->_getParam('page') : 1;
        $perpage = ($this->_getParam('perpage')) ? $this->_getParam('perpage') : 25;
        if ($perpage == 'all') $perpage = -1;
		
		if( $perpage < 1 )
        {
            $results = array(
                'pager' => null,
                'results' => $query->execute(),
            );
        }
        else
        {
            $pager = new Doctrine_Pager($query, $page, $perpage);
        
            $results = array(
                'pager' => $pager,
                'results' => $pager->execute(),
            );
        }
		
		$this->view->requests = $results;
	}
	
	/**
	 * Work order management
	 */
	
	public function viewAction()
	{
        $request_id = intval($this->_getParam('id'));
        $workorder = MaintenanceWorkorder::find($request_id);
        
        if (!$workorder)
            throw new DF_Exception('Work order #'.$request_id.' not found!');
        
        $progress = $workorder->getProgress();
        if ($progress)
            $this->view->progress = $progress;
        $this->view->workorder = $workorder;
        
        $charges = array();
        if ($workorder->Transactions->count() > 0)
        {
            foreach($workorder->Transactions as $trans)
            {
                $trans_amount = 0;
                foreach($trans->Splits as $split)
                {
                    $trans_amount += $split->split_amount;
                }
                
                $charges[] = $trans->toArray() + array('amount' => $trans_amount);
            }
        }
        $this->view->charges = $charges;
        
        
        $this->view->status = $this->_getParam('status');
	}
    
    public function printAction()
    {
        $request_id = intval($this->_getParam('id'));
		$workorder = MaintenanceWorkorder::find($request_id);
        
		if (!$workorder)
			throw new DF_Exception('Work order #'.$request_id.' not found!');
        
        if ($this->_hasParam('task_id'))
        {
            $task_id = intval($this->_getParam('task_id'));
            $task = MaintenanceTask::find($task_id);
            
            if (!$task)
                    throw new DF_Exception('Task #'.$task_id.' not found!');
            
            $this->view->task = $task;
            $this->view->print_task = true;
        }
        
        $this->view->workorder = $workorder;
    }
	
	public function editAction()
	{
		$form = new \DF\Form($this->current_module_config->forms->maintenance->form);
        
		$id = intval($this->_getParam('id'));
		if ($id != 0)
		{
            $record = MaintenanceWorkorder::find($id);
            $form->setDefaults($record->toArray());
		}
        else
        {
            $user = \DF\Auth::getInstance()->getLoggedInUser();
            $form->setDefaults(array(
                'submitter_name' => $user->getNameFirstLast(),
                'submitter_email' => $user->email,
                'submitter_phone_number' => $user->phone,
                'status' => 1,
            ));
            $record = new MaintenanceWorkorder();
            $record->time_reported = DF_TIME;
        }
		
		if (!empty($_POST) && $form->isValid($_POST))
		{
			$data = $form->getValues();
			
			$record->asset_id = (int)$data['asset_id'];
            $record->deferred_to = $data['deferred_to'];
			$record->location = $data['location'];
			$record->origin = $data['origin'];
			$record->workorder_category_id = ($data['workorder_category_id'] > 0 ? (int)$data['workorder_category_id'] : NULL);
			$record->description = $data['description'];
			$record->submitter_name = $data['submitter_name'];
			$record->submitter_phone_number = $data['submitter_phone_number'];
			$record->submitter_email = $data['submitter_email'];
            $record->time_started = $data['time_started'];
            $record->time_done = $data['time_done'];
            $record->time_spent = floatval($data['time_spent']);
            $record->notes = $data['notes'];
            
            if ($id != 0)
            {
                $record->setStatus($data['status']);
            }
            else
            {
                $record->status = (int)$data['status'];
            }
            
            try
            {
                $record->save();
                
                $this->flash('Work order updated!');
                $this->redirectFromHere(array('action' => 'view', 'id' => $record->id));
            }
            catch(Doctrine_Exception $e)
            {
                $this->alert('Error: '.$e->getMessage());
            }
		}
		
		$this->view->form = $form;
	}

    public function createmakereadyAction()
    {
        $form = new \DF\Form($this->current_module_config->forms->maintenance_makeready);
        $user = \DF\Auth::getInstance()->getLoggedInUser();
        $form->setDefaults(array(
            'submitter_name' => $user->getNameFirstLast(),
            'submitter_phone_number' => $user->phone,
            'submitter_email' => $user->email,
        ));
        if ((!empty($_POST)) && ($form->isValid($_POST)))
        {
            $data = $form->getValues();
            
            $options = array();
            $options['asset_id'] = $data['asset_id'];
            $options['location'] = $data['location'];
            $options['origin'] = 'movein';
            $options['workorder_category_id'] = 1;
            $options['description'] = 'Make Ready';
            $options['submitter_name'] = $data['submitter_name'];
            $options['submitter_phone_number'] = $data['submitter_phone_number'];
            $options['submitter_email'] = $data['submitter_email'];
            
            $makeready = MaintenanceWorkorder::createFromTemplate($options);
            
            try
            {
                $makeready->save();
                $this->flash('Make Ready created');
                $this->redirect(\DF\Url::route(array('module' => 'maintenance', 'controller' => 'manage', 'action' => 'view', 'id' => $makeready->id)));
            }
            catch(Doctrine_Exception $e)
            {
                $this->alert($e->getMessage());
            }
        }
        
        $this->view->form = $form;
    }
    
    public function setstatusAction()
    {
        $id = intval($this->_getParam('id'));
		$record = MaintenanceWorkorder::find($id);
		
		if (!$record)
			throw new DF_Exception('Work order #'.$id.' not found!');
		
        $new_status = (int)$this->_getParam('status');
        $record->setStatus($new_status);
        $record->save();
        
        $this->alert('Status updated.');
		$this->redirectFromHere(array('action' => 'view', 'id' => $record->id));
    }
	
	public function deleteAction()
	{
		$id = intval($this->_getParam('id'));
		$record = MaintenanceWorkorder::find($id);
		
		if (!$record)
			throw new DF_Exception('Work order #'.$id.' not found!');
		
		$record->status = MaintenanceWorkorder::STATUS_DELETED;
		$record->save();
		
		$this->alert('Record flagged as deleted.');
		$this->redirectFromHere(array('action' => 'view', 'id' => $record->id));
	}
	
	/**
	 * Work order task management
	 */
	
	public function edittaskAction()
	{
		$form = new \DF\Form($this->current_module_config->forms->maintenance_task->form);
		
		$workorder_id = intval($this->_getParam('id'));
        $workorder = MaintenanceWorkorder::find($workorder_id);
		$task_id = intval($this->_getParam('task_id'));
		
		if ($task_id != 0)
		{
			$record = MaintenanceTask::find($task_id);
			$form->setDefaults($record->toArray());
		}
		
		if (!empty($_POST) && $form->isValid($_POST))
		{
			$data = $form->getValues();
			
			if (!$record)
				$record = new MaintenanceTask();
			
			$record->workorder_id = $workorder_id;
			$record->job_description_id = intval($data['job_description_id']) > 0 ? intval($data['job_description_id']) : null;
			$record->description = $data['description'];
            $record->time_started = intval($data['time_started']) > 0 ? intval($data['time_started']) : null;
            $record->time_done = intval($data['time_done']) > 0 ? intval($data['time_done']) : null;
            $record->time_spent = floatval($data['time_spent']) > 0 ? floatval($data['time_spent']) : null;
			$record->status = intval($data['status']);
			$record->comments = $data['comments'];
            
            try
            {
                $record->save();
                if ($task_id == 0)
                    $task_id = $record->id;
                
                if ($workorder instanceof MaintenanceWorkorder)
                    $workorder->updateTimeSpent();
                
                $this->flash('Work order task updated!');
                $this->redirectFromHere(array('action' => 'view', 'id' => $workorder_id, 'task_id' => NULL, '#target' => 'task_'.$task_id));
            }
            catch(Doctrine_Exception $e)
            {
                $this->alert('Error: '.$e->getMessage());
            }
		}
		
		$this->view->form = $form;
	}

    public function deletetaskAction()
    {
        \DF\Csrf::validateToken($this->_getParam('csrf'));
        $task_id = $this->_getParam('task_id');
        $record = MaintenanceTask::find($task_id);
        if ($record)
        {
            $workorder = MaintenanceWorkorder::find($record->workorder_id);
            try
            {
                $record->delete();
                if ($workorder instanceof MaintenanceWorkorder)
                    $workorder->updateTimeSpent();
                
                $this->flash('Task deleted');
            }
            catch(Doctrine_Exception $e)
            {
                $this->alert($e->getMessage());
            }
        }
        $this->redirectToReferrer();
    }
    
    public function findtaskpartAction()
    {
        $this->doNotRender();
        
        $query = $this->_getParam('query');
        $results = array();
        
        if (strlen($query) > 2)
        {
            $results_array = Doctrine_Query::create()
                ->from('MaintenanceInventoryPart p')
                ->addWhere('p.name LIKE ? OR p.part_number LIKE ?', array('%'.$query.'%', '%'.$query.'%'))
                ->orderBy('p.name ASC')
                ->execute(array(), Doctrine_Core::HYDRATE_ARRAY);
            
            if ($results_array)
            {
                foreach($results_array as $result)
                {
                    $results[] = array(
                        'id' => $result['id'],
                        'text' => "{$result['name']} [part #{$result['part_number']}]",
                    );
                }
            }
        }
        
        echo Zend_Json::encode(array('status' => 'success', 'results' => $results, 'results_num' => count($results)));
    }
	
	public function addtaskpartAction()
	{
		$task = MaintenanceTask::find($this->_getParam('task_id'));
		if (!$task)
			throw new DF_Exception('Maintenance task not found!');
		
		$part = MaintenanceInventoryPart::find($this->_getParam('part_id'));
		if (!$part)
			throw new DF_Exception('Inventory part not found!');
		
		$task->InventoryParts[] = $part;
		$task->save();
		
		$this->alert('Part added!');
		$this->redirectFromHere(array('action' => 'view', '#target' => 'task_'.$task->id));
		return;
	}
	
	public function deletetaskpartAction()
	{
		$q = Doctrine_Query::create()
			->delete('MaintenanceTaskParts')
			->addWhere('maintenance_task_id = ?', $this->_getParam('task_id'))
			->addWhere('inventory_part_id = ?', $this->_getParam('part_id'))
			->execute();
		
		$this->alert('Part deleted.');
		$this->redirectFromHere(array('action' => 'view', 'task_id' => NULL, 'part_id' => NULL, '#target' => 'task_'.$task->id));
		return;
	}
	
	public function addtaskworkerAction()
	{
		$task = MaintenanceTask::find($this->_getParam('task_id'));
		if (!$task)
			throw new DF_Exception('Maintenance task not found!');
		
		$worker = MaintenanceWorker::find($this->_getParam('worker_id'));
		if (!$worker)
			throw new DF_Exception('Worker not found!');
		
		$task->Workers[] = $worker;
		$task->save();
		
		$this->alert('Worker added!');
		$this->redirectFromHere(array('action' => 'view', '#target' => 'task_'.$task->id));
		return;
	}
	
	public function deletetaskworkerAction()
	{
		$q = Doctrine_Query::create()
			->delete('MaintenanceTaskWorkers')
			->addWhere('maintenance_task_id = ?', $this->_getParam('task_id'))
			->addWhere('worker_id = ?', $this->_getParam('worker_id'))
			->execute();
		
		$this->alert('Worker deleted.');
		$this->redirectFromHere(array('action' => 'view', 'task_id' => NULL, 'worker_id' => NULL, '#target' => 'task_'.$task->id));
		return;
	}

    public function addworkorderpartAction()
	{
		$wo = MaintenanceWorkorder::find($this->_getParam('workorder_id'));
		if (!$wo)
			throw new DF_Exception('Work Order not found!');

		$part = MaintenanceInventoryPart::find($this->_getParam('part_id'));
		if (!$part)
			throw new DF_Exception('Inventory part not found!');

		$wo->InventoryParts[] = $part;
		$wo->save();
        
		$this->alert('Part added!');
		$this->redirectFromHere(array('action' => 'view'));
		return;
	}

	public function deleteworkorderpartAction()
	{
		$q = Doctrine_Query::create()
			->delete('MaintenanceWorkorderParts')
			->addWhere('workorder_id = ?', $this->_getParam('workorder_id'))
			->addWhere('inventory_part_id = ?', $this->_getParam('part_id'))
			->execute();
        
		$this->alert('Part deleted.');
		$this->redirectFromHere(array('action' => 'view', 'task_id' => NULL, 'part_id' => NULL));
		return;
	}

	public function addworkorderworkerAction()
	{
		$wo = MaintenanceWorkorder::find($this->_getParam('workorder_id'));
		if (!$wo)
			throw new DF_Exception('Work Order not found!');
        
		$worker = MaintenanceWorker::find($this->_getParam('worker_id'));
		if (!$worker)
			throw new DF_Exception('Worker not found!');
        
		$wo->Workers[] = $worker;
		$wo->save();
        
		$this->alert('Worker added!');
		$this->redirectFromHere(array('action' => 'view'));
		return;
	}

	public function deleteworkorderworkerAction()
	{
		$q = Doctrine_Query::create()
			->delete('MaintenanceWorkorderWorkers')
			->addWhere('workorder_id = ?', $this->_getParam('workorder_id'))
			->addWhere('worker_id = ?', $this->_getParam('worker_id'))
			->execute();
        
		$this->alert('Worker deleted.');
		$this->redirectFromHere(array('action' => 'view', 'workorder_id' => NULL, 'worker_id' => NULL));
		return;
	}
	
	/**
	 * Submit charges to ledger.
	 */
	
	public function postchargesAction()
	{
		$id = intval($this->_getParam('id'));
		$record = MaintenanceWorkorder::find($id);
		
		$amount = (float)$this->_getParam('amount');
		$description = $this->_getParam('description');
		$charge_to = $this->_getParam('charge_to');
		
		$residents_to_charge = array();
		
		if ($charge_to == "individual")
		{
			// Charge to a single resident ID.
			if ($record->User->resident->id)
				$residents_to_charge[] = $record->User->resident->id;
		}
		else
		{
			// Charge to a series of residents.
			$occupancies_in_range = Doctrine_Query::create()
				->from('Occupancy o')
				->addWhere('o.asset_id = ?', $record->asset->id)
				->addWhere('o.start_date < ?', time())
				->addWhere('o.end_date IS NULL OR o.end_date > ?', time())
				->orderBy('o.id DESC')
				->execute(array(), Doctrine_Core::HYDRATE_ARRAY);
			
			foreach((array)$occupancies_in_range as $occupancy)
			{
				$residents_to_charge[] = $occupancy['resident_id'];
			}
		}
		
		if ($residents_to_charge)
		{
			$num_residents = count($residents_to_charge);
			$item_id = Item::fetchByName('Repair/Maintenance');
			
			// Post new transaction.
			$transaction = new RegisterTransaction();
			$transaction->posted = time();
			$transaction->date = time();
			$transaction->transaction_type = RegisterTransaction::TRANSACTION_TYPE_DAMAGES;
			$transaction->asset_id = $record->asset->id;
            $transaction->workorder_id = $record->id;
			$transaction->memo = 'Maintenance Charges - '.$description;
			$transaction->save();
			$transaction_id = $transaction->id;
			
			foreach($residents_to_charge as $resident_id)
			{
				$resident_amount = round($amount / $num_residents, 2);
				
				// Post new split for this resident.
				$split = new Split();
				$split->posted = time();
				$split->transaction_id = $transaction_id;
				$split->resident_id = $resident_id;
				$split->ledger_id = Split::LEDGER_STANDARD;
				$split->credit_or_debit = 'D';
				$split->item_id = $item_id;
				$split->asset_id = $record->asset->id;
				$split->split_amount = 0-$resident_amount;
				$split->save();
				
				Split::updateRunningBalance($resident_id, $split->ledger_id);
			}
		}
		
		$this->alert('Charges posted!');
		$this->redirectFromHere(array('action' => 'view'));
	}

    public function findresidentAction()
    {
        $this->doNotRender();
        
        $query = $this->_getParam('query');
        $results = array();
        
        if (strlen($query) >= 2)
        {
            $results_array = Doctrine_Query::create()
                ->from('Resident r')
                ->orWhere('r.first_name LIKE ?', '%'.$query.'%')
                ->orWhere('r.surname LIKE ?', '%'.$query.'%')
                ->orWhere('r.uin LIKE ?', '%'.$query.'%')
                ->orWhere('r.email LIKE ?', '%'.$query.'%')
                ->orderBy('r.surname ASC')
                ->execute(array(), Doctrine_Core::HYDRATE_ARRAY);
            
            // Special handling for the '(All)' listings.
            if (strstr($query, '(All)') !== FALSE)
            {
                $apt_name = str_replace(' (All)', '', $query);
                
                $apts_matches = Doctrine_Query::create()
                    ->from('Asset a')
                    ->where('a.name LIKE ?', $apt_name)
                    ->execute();
            }
            else
            {
                $apts_matches = Doctrine_Query::create()
                    ->from('Asset a')
                    ->orWhere('a.name LIKE ?', '%'.$query.'%')
                    ->orWhere('a.description LIKE ?', '%'.$query.'%')
                    ->execute();
            }

            if ($results_array)
            {
                foreach($results_array as $result)
                {
                    if (!$result['deleted_at'])
                    {
                    $results[] = array(
                        'email' => $result['email'],
                        'name' => join(' ', array_filter(array($result['first_name'], $result['middle_name'], $result['surname']))),
                        'phone' => $result['primary_phone'],
                        'text' => $result['surname'].', '.join(' ',array($result['first_name'],$result['middle_name'])).' ('.$result['uin'].')',
                        );
                    }
                }
            }

            if ($apts_matches)
            {
                $apt_results = array();
                foreach($apts_matches as $apt)
                {
                    $residents = $apt->getAllResidents();
                    if (count($residents) > 0)
                    {
                        foreach($residents as $res)
                        {
                            $resident = $res->toArray();
                            $apt_results[] = array(
                                'email' => $resident['email'],
                                'name' => join(' ', array_filter(array($resident['first_name'], $resident['middle_name'], $resident['surname']))),
                                'phone' => $resident['primary_phone'],
                                'text' => $apt->name.': '.$resident['surname'].', '.join(' ',array($resident['first_name'],$resident['middle_name'])).' ('.$resident['uin'].')',
                            );
                        }
                    }
                }
                if (count($apt_results) > 0)
                    $results = array_merge($results, $apt_results);
            }
        }

        echo Zend_Json::encode(array('status' => 'success', 'results' => $results, 'results_num' => count($results)));

    }
}