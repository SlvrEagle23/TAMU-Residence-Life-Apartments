<?php
class Maintenance_IndexController extends \DF\Controller\Action
{
	public function permissions()
	{
		return \DF\Acl::getInstance()->isAllowed('is logged in');
	}
	
    public function indexAction()
    {
		$acl = \DF\Acl::getInstance();
		
		if ($acl->isAllowed('manage maintenance'))
			$this->redirectFromHere(array('controller' => 'manage', 'action' => 'index'));
		else
			$this->redirectFromHere(array('controller' => 'submit', 'action' => 'history'));
	}
}