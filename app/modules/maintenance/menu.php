<?php
/**
 * Maintenance module menu configuration
 */

return array(
    'default' => array(
		'maintenance'	=> array(
			'label'		=> 'Maintenance',
			'module'	=> 'maintenance',
            'permission' => 'is logged in',
            
            'pages'     => array(
                'submit'        => array(
                    'label'         => 'Submit Request',
                    'module'        => 'maintenance',
                    'controller'    => 'submit',
                    'action'        => 'index',
                    'permission'    => 'is logged in',
                ),
                
                'manage'        => array(
                    'label'         => 'Manage Maintenance',
                    'module'        => 'maintenance',
                    'controller'    => 'manage',
                    'action'        => 'index',
                    'permission'    => 'manage work orders',
                    
                    'pages'         => array(
                        'manage_requests'       => array(
                            'module'    => 'maintenance',
                            'controller' => 'manage',
                            'action'    => 'requests',
                        ),
                        'manage_view'           => array(
                            'module'    => 'maintenance',
                            'controller' => 'manage',
                            'action'    => 'view',
                        ),
                        
                        'parts'                 => array(
                            'module'    => 'maintenance',
                            'controller' => 'parts',
                            'action'    => 'index',
                            'label'     => 'Manage Parts',
                            'permission' => 'manage work orders',
                        ),
                        
                        'workers'                 => array(
                            'module'    => 'maintenance',
                            'controller' => 'workers',
                            'action'    => 'index',
                            'label'     => 'Manage Workers',
                            'permission' => 'manage work orders',
                        ),
                    ),
                ),
                
                'reports' => array(
                    'label' => 'Maintenance Reports',
                    'module' => 'maintenance',
                    'controller' => 'reports',
                    'action' => 'index',
                    
                    'pages' => array(
                        'report_pages' => array(
                            'module' => 'maintenance',
                            'controller' => 'reports',
                        ),
                    ),
                ),
                
                'electric' => array(
                    'label'     => 'Electric Meter Readings',
                    'module'    => 'maintenance',
                    'controller' => 'electric',
                    'permission' => 'manage electric meter readings',
                ),
            ),
		),
    ),
);