<?php
class Api_ErrorController extends \DF\Controller\Action_Api
{
	public function permissions()
	{
		return TRUE;
	}
	
    public function errorAction()
    {
        // Grab the error object from the request
        $errors = $this->_getParam('error_handler');
        
        $error_response = array(
            'error' => true,
            'code' => 500,
            'message' => $errors->exception->getMessage(),
        );
        
        switch ($errors->type)
        {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
				$error_message['code'] = 404;
                $error_response['message'] = "Page not found";
            break;
                
            default:
                if($errors->exception instanceOf \DF\Exception\AccessDenied)
                    $error_response['code'] = 401;
                else
                    $error_response['code'] = 500;
            break;
        }
        
        if (!$error_response['message'])
			$error_response['message'] = 'An error occurred ('.get_class($errors->exception).').';
        
        $this->getResponse()->setHttpResponseCode($error_response['code']);
        $this->output($error_response['message'], 'ERROR');
        return;
    }
}