<?php
use \Entity\User;

class AccountController extends \DF\Controller\Action
{
    public function indexAction()
    {
        $this->redirectHome();
        return;
    }

    public function profileAction()
    {
        $user = $this->auth->getLoggedInUser();
        $resident = $user->resident;

        if (!$resident)
            throw new \DF\Exception\DisplayOnly('Could not find resident profile');

        if ($resident->isExternal())
            $this->redirectHome();

        $this->view->mode = 'edit_profile';
        $this->_forward('view', 'residents', 'management', array('id' => $resident->id));
    }
    
    public function loginAction()
    {
        $this->doNotRender();
        
        if (!isset($_REQUEST['ticket']))
            $this->storeReferrer('login');
        
        $auth_result = \DF\Auth::authenticate();
        
        if ($auth_result)
            $this->redirectToStoredReferrer('login');
        else
            $this->redirectToRoute(array('module' => 'default'));
        return;
    }

    public function logoutAction()
    {
        $auth = \DF\Auth::logout();
        session_unset();

        $this->redirectToRoute(array('module' => 'default'));
    }

    public function masqueradeAction()
    {
        $this->acl->checkPermission('administer all');

        $uin = (int)$this->_getParam('uin');
        $user = User::getOrCreate($uin);

        $this->auth->masqueradeAsUser($user);

        $this->redirectHome();
        return;
    }

    public function endmasqueradeAction()
    {
        if ($this->auth->isMasqueraded())
            $this->auth->endMasquerade();

        $this->redirectHome();
        return;
    }
}