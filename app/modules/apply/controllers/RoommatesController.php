<?php
class Apply_RoommatesController extends \DF\Controller\Action
{
    public function permissions()
    {
        \DF\Auth::getInstance()->login();
        return \DF\Acl::getInstance()->isAllowed('is logged in');
    }

    public function indexAction()
    {
        
    }

    /**
     * New roommate invitation
     */
    public function inviteAction()
    {
        $user = \DF\Auth::getInstance()->getLoggedInUser();
        
        // invitation form
        $form = new \DF\Form($this->current_module_config->forms->roommate);

        if( !empty($_POST) && $form->isValid($_POST) )
        {
            $data = $form->getValues();
            $request = new RoommateRequest();

            $request->request_received = DF_TIME;
            $request->requesting_resident_id = $user->resident->id;
            $request->new_roommate_email = $data['email'];
            $request->asset_id = $data['asset_id'];

            $request->token = \DF\Csrf::getToken();

            // check for existing roommates
            $apt = Asset::find($data['asset_id']);
            $residents = $apt->getPrincipalResidents();
            $roommate_approvals = array();
            foreach($residents as $resident)
            {
                if ($resident->id != $user->resident->id)
                    $roommate_approvals[(string)$resident->id] = 'pending';
            }
            if (count($roommate_approvals) > 0)
            {
                $request->setApprovals($roommate_approvals);
            }
            else
            {
                // no roommates yet, set as approved
                $request->approved = DF_TIME;
            }

            try
            {
                $request->save();
                $request->sendInvite();

                $msg = 'Request submitted successfully.';
                if (count($roommate_approvals) > 0) $msg .= ' All other unit residents will need to approve the request.';
                $this->flash($msg);
                $this->redirectToRoute(array('module'=>'default','controller'=>'index'));
            }
            catch(Doctrine_Exception $e)
            {
                $this->alert($e->getMessage());
            }
        }
        
        $this->view->form = $form;
    }

    public function acceptAction()
    {
        $id = $this->_getParam('id');
        $email = urldecode($this->_getParam('email'));
        $token = $this->_getParam('token');

        $invite = RoommateRequest::find($id);

        if ($invite instanceof RoommateRequest)
        {
            if (($invite->new_roommate_email != $email) || ($invite->token != $token))
            {
                throw new \DF\Exception\DisplayOnly('We could not verify this roommate request');
            }
            else
            {
                $user = \DF\Auth::getInstance()->getLoggedInUser();
                // Create associated Resident record if nonexistent.
                if (!$user->resident->id)
                {
                    $resident = new Resident();
                    $resident->uin = $user->uin;
                    $resident->first_name = $user->firstname;
                    $resident->surname = $user->lastname;
                    $resident->email = $user->email;
                    $resident->primary_phone = $user->phone;
                    $resident->save();
                    $user->resident = $resident;
                    $user->save();
                }
                $invite->resident_id = $user->resident->id;
                $invite->save();
                $this->flash('Roommate request verified. Please complete the housing application process.');
                
                \DF\Messenger::send(array(
                    'to'        => 'university-apartments@housing.tamu.edu',
                    'subject'   => 'Roommate invitation accepted',
                    'template'  => 'admin_notifications/invite',
                    'vars'      => array(
                        'invite'    => $invite,
                    ),
                ));
                
                $this->redirectToRoute(array('module' => 'apply', 'controller' => 'index'));
            }
        }
    }

    public function approvalAction()
    {
        \DF\Csrf::validateToken($this->_getParam('csrf'));

        $this->doNotRender();
        $user = \DF\Auth::getInstance()->getLoggedInUser();
        $id = $this->_getParam('id');
        $invite = RoommateRequest::find($id);
        $approval = $this->_getParam('approval');

        if ($invite instanceof RoommateRequest)
        {
            $approvals = $invite->getApprovals();
            if ($approval == 'true')
            {
                $approvals[$user->resident->id] = true;
                $this->flash('Roommate request approved');
            }
            else
            {
                $approvals[$user->resident->id] = false;
                $this->flash('Roommate request rejected');
            }
            $invite->setApprovals($approvals);

            if ($invite->isApproved())
            {
                // all approvals collected
                $invite->approved = DF_TIME;
                $invite->sendInvite();
            }
            $invite->save();
        }
        $this->redirectToReferrer();
    }
}