<?php
class Apply_RenewController extends \DF\Controller\Action
{
    public function permissions()
    {
        return \DF\Acl::getInstance()->isAllowed('is logged in');
    }

    public function indexAction()
    {
        $user = \DF\Auth::getInstance()->getLoggedInUser();
        $resident = $user->resident;

        if (!$resident->id)
            throw new \DF\Exception\DisplayOnly('You are not currently listed as a resident.');

        // Check for pending applications.
        $applications = $user->resident->getPendingApplications();
        if ($applications)
            throw new \DF\Exception\DisplayOnly('You already have pending applications in the University Apartments system. For more information, contact the University Apartments staff.');

        // Find the user's active occupancy.
        $occupancies = Occupancy::getActiveByResidentId($user->resident->id);

        if (count($occupancies) == 0)
            throw new \DF\Exception\DisplayOnly('You do not have any currently active occupancies. You must be living in a current apartment to renew it using this process.');

        // Check for upcoming expiration.
        $occupancy = $occupancies[0];
        if (!$occupancy->canRenew())
            throw new \DF\Exception\DisplayOnly('Your current lease is not eligible for renewal. For more information, contact University Apartments.');

        // Handle display and processing of renewal.
        $this->view->occupancy = $occupancy;

        if ($this->_getParam('step', 'intro') == "process")
        {
            $application = new Application();
            $application->application_date = DF_TIME;
            $application->application_status_id = Application::STATUS_RENEWAL;
            $application->is_renewal = 1;
            $application->resident_id = $resident->id;
            $application->occupancy_id = $occupancy->id;

            $lease_length = (int)$_POST['requested_lease_length'];
            $application->requested_lease_length = $lease_length;

            $occupancy_model = (int)$_POST['requested_occupancy_model'];
            if ($occupancy_model != 0)
                $application->requested_occupancy_model = $occupancy_model;

            $application->save();

            $this->alert('<b>Your lease renewal request has been submitted.</b><br>You will be contacted by e-mail with any updates.', 'green');
            $this->redirectToRoute(array('module' => 'default'));
            return;
        }
    }
}