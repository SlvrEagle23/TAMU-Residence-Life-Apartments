<?php
class Apply_LeaseController extends \DF\Controller\Action
{
    public function permissions()
    {
        return \DF\Acl::getInstance()->isAllowed('is logged in');
    }

    public function indexAction()
    {
        $user = \DF\Auth::getInstance()->getLoggedInUser();
        $resident_id = $user->resident->id;

        $latest_offer = \Entity\Offer::fetchMostRecentByResidentId($resident_id);

        if (!($latest_offer instanceof Offers))
            throw new \DF\Exception\DisplayOnly('You do not have an offer on file. Leases can only be viewed if your account has a prior offer and lease agreement on file.');
        
        if (!$latest_offer->contract_data)
            throw new \DF\Exception\DisplayOnly('You did not complete your lease signing process online. Contact the University Apartments office to retrieve a copy of the paper lease you originally signed.');

        $lease_info = LeaseVersion::buildFromOffer($latest_offer);
        $this->view->lease = $lease_info;
    }
}