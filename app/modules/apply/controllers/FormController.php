<?php
class Apply_FormController extends \DF\Controller\Action
{
    public function permissions()
    {
        \DF\Auth::getInstance()->login();
        return \DF\Acl::getInstance()->isAllowed('is logged in');
    }

    /**
     * New tenant application
     */
    public function indexAction()
    {
        $this->enforceSSL();
        $user = \DF\Auth::getInstance()->getLoggedInUser();
        
        $compass = new DF_Service_Compass();
        $student_record = $compass->getStudentById($user->uin);
        
        // Eligibility Check
        if (!\DF\Acl::getInstance()->isAllowed('administer'))
        {
            $required_classification = Settings::getSetting('application_classification', 'X0');
            $required_year = intval(substr($required_classification, 1));
            
            if ($required_year != 0 && isset($student_record['classification']))
            {
                $student_year = intval(substr($student_record['classification'], 1));
                
                if ($student_year < $required_year)
                    throw new \DF\Exception\DisplayOnly('Not Eligible to Apply: Your classification does not meet the minimum requirements to submit an application.');
            }
            else if ($required_year != 0)
            {
                throw new \DF\Exception\DisplayOnly('Not Eligible to Apply: You do not currently have a student record.');
            }
        }
        
        // Create associated Resident record if nonexistent.
        if (!$user->resident->id)
        {
            $resident = new Resident();
            $resident->uin = $user->uin;
            $resident->first_name = $user->firstname;
            $resident->surname = $user->lastname;
            $resident->email = $user->email;
            $resident->primary_phone = $user->phone;
            $resident->save();
            $user->resident = $resident;
            $user->save();
        }
        
        // Populate select fields from student record, if applicable.
        if ($student_record && !$user->resident->classification)
        {
            $user->resident->classification = $student_record['classification'];
            $user->resident->Major = Major::getOrCreateByCode($student_record['major1']);
            $user->resident->College = College::getOrCreateByCode($student_record['college']);
			$user->resident->ethnicity = $student_record['primary_ethnicity'];
            $user->save();
        }

        // check if there is a roommate request
        $roommate_request = RoommateRequest::fetchByResidentId($user->resident->id);
        if ($roommate_request)
        {
            $is_roommate = true;
        }
        else
        {
            $is_roommate = false;
        }
        
        // application form
        $appeditform = new \DF\Form($this->current_module_config->forms->application_edit);
        // remove admin-level fields for end users
        $appeditform->removeElement('application_date');
        $appeditform->removeElement('application_status_id');
	    $appeditform->removeElement('response_deadline');
        $appeditform->removeElement('submit');
        $appform = new Zend_Form_SubForm();
        $appform->setLegend('Other')
                ->addElements($appeditform->getDisplayGroup('application_details')->getElements());
        $appform->addElement('select','birth_country',array(
            'label' => 'Country of Origin',
            'multiOptions' => Country::getAll(true),
            'required' => true,
        ));
        $appform->addElement('checkbox', 'international_student', array(
            'label' => 'Are you an International Student?',
        ));
        
        $id = intval($this->_getParam('id'));
        $application = Application::find($id);
        if (!$application instanceof Application)
        {
            $application = new Application();
            $application->application_date = DF_TIME;
            $application->application_status_id = Application::STATUS_WAITING;
        }
        else
        {
            $appform->setDefaults(array(
                'desired_move_in' => $application->desired_move_in,
                'floor_preference' => $application->floor_preference,
                'special_needs' => $application->special_needs,
                'military_vet' => $application->military_vet,
                'police_fireprotection' => $application->police_fireprotection,
            ));
        }

        // get resident form
        $resform = new UA_Custom_Form_Resident_Edit($user->resident);
        $resinfo = $resform->getSubForm('resident_info_subform');
        $resinfo->removeElement('principal_resident');
        $resinfo->removeElement('laundry_card');
        $resinfo->removeElement('orientation_date');
        $resinfo->removeElement('uin_sub_id');
        $contact = $resform->getSubForm('contact_info_subform');
        $address = $resform->getSubForm('mailing_address_subform');
        $choices = new Zend_Form_Subform();
        $choices->setLegend('Apartment Styles')
                ->setDescription('Select your first, second, and subsequent choices below:<br>*If you will have a roommate, roommates must select same type of contract');
        $choices->setElementDecorators(array(
            'ViewHelper',
            array(array('data' => 'HtmlTag'), array('tag' => 'span', 'class' => 'element')),
            array('Label', array('tag' => 'span')),
            array(array('row' => 'HtmlTag'), array('tag' => 'div')),
        ));
        $apt_styles = array();
        foreach(AssetType::fetchApartmentTypes() as $id => $type)
        {
            $occupancy_model = (!empty($type['OccupancyModel']['name'])) ? $type['OccupancyModel']['name'] : 'other';
            $apt_styles[$occupancy_model][$id] = $type;
        }
        ksort($apt_styles);
        foreach($apt_styles as $style => $types)
        {
            if ($style != 'other')
                $header_txt = 'Available by the '.ucwords($style);
            else
                $header_txt = 'Other';
            
            $header = new \DF\Form_Element_Markup('header_'.$style, array(
                'attribs' => array(
                    'markup' => "<strong>$header_txt</strong><br/>",
                ),
            ));
            $choices->addElement($header);
            
            foreach($types as $id => $type)
            {
                $choices->addElement('select','type_'.$id, array(
                    'label' => $type['description'],
                    'multiOptions' => array(
                        '0' => '',
                        '1' => '1',
                        '2' => '2',
                        '3' => '3',
                        '4' => '4',
                        '5' => '5',
                        '6' => '6',
                        '7' => '7',
                    ),
                ));
            }
        }
	$gp2_msg = new \DF\Form_Element_Markup('gp2', array(
	    'attribs' => array(
		'markup' => '<p><strong>To apply to live in Gardens 2, please visit the <a href="http://reslife.tamu.edu/how/apply/">Residence Life Website</a></strong></p>',
	    ),
	));
	$choices->addElement($gp2_msg);
	
$chart_html = <<<CHART_HTML
<table>
    <tr>
    <th></th>
    <th>Avenue A<br/>College View<br/>Hensel</th>
    <th>Gardens 1<br/>(Entire Apartment)</th>
    <th>Gardens 1<br/>(Individual Bedroom)</th>
    <th>Gardens 2<br/>(Individual Bed)</th>
    </tr>
    <tr>
    <th>Month to Month Contract</th>
    <td>X</td>
    <td></td>
    <td></td>
    <td></td>
    </tr>
    <tr>
    <th>9 Month Contract</th>
    <td></td>
    <td>X</td>
    <td>X</td>
    <td></td>
    </tr>
    <tr>
    <th>12 Month Contract</th>
    <td></td>
    <td>X</td>
    <td>X</td>
    <td></td>
    </tr>
    <tr>
    <th>9 Month w/ Summer Option</th>
    <td></td>
    <td></td>
    <td></td>
    <td>X</td>
    </tr>
    <tr>
    <th>Responsible for Apartment Rent</th>
    <td>X</td>
    <td>X</td>
    <td></td>
    <td></td>
    </tr>
    <tr>
    <th>Responsible for Bedroom Rent</th>
    <td></td>
    <td></td>
    <td>X</td>
    <td></td>
    </tr>
    <tr>
    <th>Responsible for Bed Rent</th>
    <td></td>
    <td></td>
    <td></td>
    <td>X</td>
    </tr>
    <tr>
    <th>Pay Rent Monthly</th>
    <td>X</td>
    <td>X</td>
    <td>X</td>
    <td></td>
    </tr>
    <tr>
    <th>Pay Rent Once A Semester</th>
    <td></td>
    <td></td>
    <td></td>
    <td>X</td>
    </tr>
    <tr>
    <th>All Utilities Included</th>
    <td></td>
    <td></td>
    <td></td>
    <td>X</td>
    </tr>
    <tr>
    <th>Pay Electricity Monthly</th>
    <td>X</td>
    <td>X</td>
    <td>X</td>
    <td></td>
    </tr>
    <tr>
    <th>Graduate Students</th>
    <td>X</td>
    <td>X</td>
    <td>X</td>
    <td>X</td>
    </tr>
    <tr>
    <th>Married Students</th>
    <td>X</td>
    <td>X</td>
    <td></td>
    <td></td>
    </tr>
    <tr>
    <th>Students with Children</th>
    <td>X</td>
    <td>X</td>
    <td></td>
    <td></td>
    </tr>
    <tr>
    <th>Undergrad Students with 30+ Hours</th>
    <td></td>
    <td>X</td>
    <td>X</td>
    <td>X</td>
    </tr>
    <tr>
    <th>International Students</th>
    <td></td>
    <td>X</td>
    <td>X</td>
    <td>X</td>
    </tr>
    <tr>
    <th>U.S. Military Veteran Students</th>
    <td></td>
    <td>X</td>
    <td>X</td>
    <td>X</td>
    </tr>
    <tr>
    <th>Students 21 Years or Older</th>
    <td></td>
    <td>X</td>
    <td>X</td>
    <td>X</td>
    </tr>
    <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td><a href="http://reslife.tamu.edu/how/apply" style="color:red;">APPLY NOW FOR GARDENS 2!</a></td>
    </tr>
</table>
CHART_HTML;
	
	$lease_chart = new \DF\Form();
	$lease_chart->setLegend('Lease Types');
	$lease_chart->addElement('markup', 'chart', array(
	    'attribs' => array(
		'markup' => $chart_html,
	    ),
	));
        

        if ($application instanceof Application)
        {
            foreach($application->WaitingList as $rank => $asset_type_id)
            {
                $choices->setDefault('type_'.$asset_type_id, $rank + 1);
            }
        }

        $emergency1 = $resform->getSubForm('emergency_contact_info_subform');
        $emergency2 = $resform->getSubForm('emergency2_contact_info_subform');
        
        $signatures = new Zend_Form_SubForm('signatures_subform');
            $signatures->setLegend('Signature');

            $signatures->addElement('hidden', 'contract_version', array(
                'value' => Settings::getSetting('contract_version'),
            ));

            $signatures->addElement('text', 'uin_signature', array(
                'label' => 'Enter UIN here',
                'required' => true,
            ));
            
            $signatures->addElement('text', 'contract_initials', array(
                'label' => 'Enter initials here',
                'required' => true,
                'description' => 'By entering my UIN and initials here I acknowledge that I have read and agree to the terms of the previously reviewed contract. I understand that this application will be discarded unless online payment is completed on the following screens.',
                'attribs' => array(
                    'size' => 4,
                ),
            ));
            
            $signatures->addElement('hidden', 'contract_version', array(
                'label' => '',
                'value' => Settings::getSetting('application_contract_version'),
            ));

        // add resident subforms to app form
        $form = new \DF\Form();
        $form->loadDefaultDecorators();
        $form->addSubForm($resinfo, $resinfo->getName());
        $form->addSubForm($contact, $contact->getName());
        $form->addSubForm($address, $address->getName());
        $form->addSubForm($appform, 'app_specific_fields');
        $form->addSubForm($emergency1, 'emergency_contact_info_subform');
        $form->addSubForm($emergency2, 'emergency2_contact_info_subform');
        if (!$is_roommate) {
	    $form->addSubForm($lease_chart, 'lease_types_subform');
	    $form->addSubForm($choices, 'apartment_types_subform');
	}
        $form->addSubForm($signatures, 'signatures_subform');
        
        $form->addElement('submit', 'submit', array(
            'label' => 'Submit Application',
            'attribs' => array(
                //'helper' => 'formButton',
                'class' => 'ui-button',
            ),
        ));
        
        if( !empty($_POST) && $form->isValid($_POST) )
        {
            $data = $form->getValues();
            $resident = $user->resident;
            
            // Resident info
            $resident->resident_type_id = $data['resident_info_subform']['resident_type_id'];
            
            $resident->first_name = $data['resident_info_subform']['first_name'];
            $resident->middle_name = $data['resident_info_subform']['middle_name'];
            $resident->surname = $data['resident_info_subform']['surname'];
            $resident->suffix = $data['resident_info_subform']['suffix'];
            $resident->uin = $data['resident_info_subform']['uin'];
            
            // Contact info
            $resident->primary_phone = $data['contact_info_subform']['primary_phone'];
            $resident->secondary_phone = $data['contact_info_subform']['secondary_phone'];
            $resident->voip_phone = $data['contact_info_subform']['voip_phone'];
            //$resident->email = $data['contact_info_subform']['email'];
            $resident->preferred_email = $data['contact_info_subform']['preferred_email'];
            
            // Mailing address
            $resident->mailing_address = $data['mailing_address_subform']['mailing_address'];
            $resident->mailing_address2 = $data['mailing_address_subform']['mailing_address2'];
            $resident->mailing_city = $data['mailing_address_subform']['mailing_city'];
            $resident->mailing_state = $data['mailing_address_subform']['mailing_state'];
            $resident->mailing_zip = $data['mailing_address_subform']['mailing_zip'];
            $resident->mailing_country = (trim($data['mailing_address_subform']['mailing_country'])!='null') ? $data['mailing_address_subform']['mailing_country'] : null;
            
            // Application-specific fields
            $application->desired_move_in = $data['app_specific_fields']['desired_move_in'];
            $application->requested_lease_length = $data['app_specific_fields']['requested_lease_length'];
            $application->floor_preference = $data['app_specific_fields']['floor_preference'];
            $application->special_needs = $data['app_specific_fields']['special_needs'];
            $application->military_vet = ((int)$data['app_specific_fields']['military_vet'] != 0) ? 1 : 0;
            $application->police_fireprotection = ((int)$data['app_specific_fields']['police_fireprotection'] != 0) ? 1 : 0;
            $resident->birth_country = (trim($data['app_specific_fields']['birth_country']) != 'null') ? $data['app_specific_fields']['birth_country'] : null;
            $resident->international_student = ((int)$data['app_specific_fields']['international_student'] != 0) ? 1: 0;

            // Emergency Contact 1
            $resident->emergency_missing_person = (trim($data['emergency_contact_info_subform']['emergency_missing_person'])!='') ? 1 : 0;
            $resident->emergency_firstname = $data['emergency_contact_info_subform']['emergency_firstname'];
            $resident->emergency_middlename = $data['emergency_contact_info_subform']['emergency_middlename'];
            $resident->emergency_lastname = $data['emergency_contact_info_subform']['emergency_lastname'];
            $resident->emergency_address = $data['emergency_contact_info_subform']['emergency_address'];
            $resident->emergency_address2 = $data['emergency_contact_info_subform']['emergency_address2'];
            $resident->emergency_city = $data['emergency_contact_info_subform']['emergency_city'];
            $resident->emergency_state = $data['emergency_contact_info_subform']['emergency_state'];
            $resident->emergency_zip = $data['emergency_contact_info_subform']['emergency_zip'];
            $resident->emergency_country = (trim($data['emergency_contact_info_subform']['emergency_country'])!='null') ? $data['emergency_contact_info_subform']['emergency_country'] : null;
            $resident->emergency_phone = $data['emergency_contact_info_subform']['emergency_phone'];
            $resident->emergency_relationship = $data['emergency_contact_info_subform']['emergency_relationship'];
            
            // Emergency Contact 2
            $resident->emergency2_missing_person = (trim($data['emergency2_contact_info_subform']['emergency2_missing_person'])!='') ? 1 : 0;
            $resident->emergency2_firstname = $data['emergency2_contact_info_subform']['emergency2_firstname'];
            $resident->emergency2_middlename = $data['emergency2_contact_info_subform']['emergency2_middlename'];
            $resident->emergency2_lastname = $data['emergency2_contact_info_subform']['emergency2_lastname'];
            $resident->emergency2_address = $data['emergency2_contact_info_subform']['emergency2_address'];
            $resident->emergency2_address2 = $data['emergency2_contact_info_subform']['emergency2_address2'];
            $resident->emergency2_city = $data['emergency2_contact_info_subform']['emergency2_city'];
            $resident->emergency2_state = $data['emergency2_contact_info_subform']['emergency2_state'];
            $resident->emergency2_zip = $data['emergency2_contact_info_subform']['emergency2_zip'];
            $resident->emergency2_country = (trim($data['emergency2_contact_info_subform']['emergency2_country'])!='null') ? $data['emergency2_contact_info_subform']['emergency2_country'] : null;
            $resident->emergency2_phone = $data['emergency2_contact_info_subform']['emergency2_phone'];
            $resident->emergency2_relationship = $data['emergency2_contact_info_subform']['emergency2_relationship'];

            if (!$is_roommate)
            {
                $resident->principal_resident = 1;
                // Apartment Style choices
                foreach($data['apartment_types_subform'] as $typeid => $value)
                {
                    $type_parts = explode('_', $typeid);
                    if (((int)$type_parts[1] > 0) && ((int)$value > 0))
                        $asset_types[(int)$value] = (int)$type_parts[1];
                }
                foreach($asset_types as $choice_rank => $asset_type_id)
                {
                    $application->WaitingList[$choice_rank - 1]->choice_asset_type_id = $asset_type_id;
                    $application->WaitingList[$choice_rank - 1]->choice_rank = $choice_rank;
                }
            }
            
            $application->resident_id = $resident->id;
            
            $user_uin = \DF\Auth::getInstance()->getLoggedInUser()->uin;
            if ($data['signatures_subform']['uin_signature'] != $user_uin)
            {
                $form->getSubForm('signatures_subform')->getElement('uin_signature')->addError('UIN entered for signature did not match logged in user.');
            }
            else if (trim($data['app_specific_fields']['desired_move_in']) == '')
            {
                $form->getSubForm('app_specific_fields')->getElement('desired_move_in')->addError('Move-in Date is a required field.');
            }
            else
            {
                try
                {
                    $application->save();
                    $resident->save();
                    if ($is_roommate)
                    {
                        $roommate_request->application_id = $application->id;
                        $roommate_request->save();
                        $template = 'apply/roommate';
                    }
                    else
                    {
                        $template = 'apply/received';
                    }
                    
                    // set up application fee transaction
                    // Post new transaction.
                    $transaction = new RegisterTransaction();
                    $transaction->posted = time();
                    $transaction->date = time();
                    $transaction->memo = 'Application Fee';
                    $transaction->save();
                    $transaction_id = $transaction->id;

                    // Post new split for this resident.
                    $split = new Split();
                    $split->posted = time();
                    $split->transaction_id = $transaction_id;
                    $split->resident_id = $resident->id;
                    $split->ledger_id = Split::LEDGER_STANDARD;
                    $split->credit_or_debit = 'D';
                    $split->item_id = Item::fetchByName('Application Fee');
                    $split->split_amount = (0-(Settings::getSetting('application_fee')));
                    $split->save();
                    
                    Split::updateRunningBalance($split->resident_id, $split->ledger_id);

                    // email confirmation
                    \DF\Messenger::send(array(
                        'to' => $resident->email,
                        'subject' => 'Thank you for applying for housing at University Apartments',
                        'template' => $template,
                    ));
		    $application_fee_amount = Settings::getSetting('application_fee');
                    $this->flash("Thanks for applying to live at the University Apartments. We are glad you have chosen to consider the University Apartments as one of your housing options. Please proceed to the payment page and pay the required $".$application_fee_amount." application fee. We will begin reviewing your application as soon as we receive the $".$application_fee_amount." application fee. If the application fee is not paid, your application is invalid and will not be considered.");
                    //$this->redirectToRoute(array('module'=>'default','controller'=>'index'));
                    $this->redirectToRoute(array('module'=>'finances','controller'=>'resident','action'=>'pay'));
                }
                catch(Doctrine_Exception $e)
                {
                    $this->alert($e->getMessage());
                }
            }
        }
        
        $this->view->form = $form;
    }
}