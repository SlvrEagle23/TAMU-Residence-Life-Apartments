<?php
use \Entity\Occupancy;

class Apply_IndexController extends \DF\Controller\Action
{
    public function permissions()
    {
        \DF\Auth::getInstance()->login();
        return \DF\Acl::getInstance()->isAllowed('is logged in');
    }

    public function indexAction()
    {
        // inital readme page
        if (\DF\Acl::getInstance()->isAllowed('is logged in'))
        {
            $user = \DF\Auth::getInstance()->getLoggedInUser();
            
            if ($user->resident->id)
            {
                $occupancies = Occupancy::getActiveByResidentId($user->resident->id);
                
                if ($occupancies)
                {
                    // resident, redirect to home
                    $this->alert('<b>You are already a resident of the University Apartments.</b><br>You do not need to complete an additional application.', 'green');
                    $this->redirect(\DF\Url::route(array('module' => 'default')));
                }
                else
                {
                    // already applied, redirect to home
                    $applications = $user->resident->getPendingApplications();
                    if ($applications)
                        $this->redirect(\DF\Url::route(array('module' => 'default')));
                }
            }
        }
    }

    public function vaccinationsAction()
    {
        // second readme page
    }
}