<?php
/**
 * Application menu configuration
 */

return array(
    'default' => array(
        
        'apply' => array(
            'label' => 'Apply Now',
            'module' => 'apply',
            'controller' => 'index',
            'permission' => 'is logged in',
            
            'pages' => array(
                'apply_form' => array(
                    'module'    => 'apply',
                    'controller' => 'form',
                ),
            ),
        ),
    ),
);