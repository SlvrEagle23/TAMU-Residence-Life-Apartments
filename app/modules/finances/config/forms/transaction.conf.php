<?php
/**
 * Transaction edit form
 */

return array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'elements'		=> array(
            
            'transaction_type' => array('select', array(
                'label' => 'Transaction Type',
                'multiOptions' => RegisterTransaction::getTypeCodes(),
            )),
            
            'date'     => array('unixdate', array(
                'label' => 'Transaction Date',
                'required' => true,
                'default' => date('m/d/Y'),
            )),
			
            'memo'      => array('textarea', array(
                'label' => 'Memo',
                'class' => 'full-width half-height',
            )),
            
            'check_reference_number' => array('text', array(
                'label' => 'Check Reference Number',
            )),
            
            'no_sale' => array('select', array(
                'label' => 'No Sale',
                'multiOptions' => array(
                    'N' => 'No',
                    'Y' => 'Yes',
                ),
            )),
            
            'register_id' => array('select', array(
                'label' => 'Register',
                'multiOptions' => Register::fetchSelect(TRUE),
            )),
            
            'reference_number' => array('text', array(
                'label' => 'Reference Number',
            )),
            
            'transaction_source_id' => array('select', array(
                'label' => 'Transaction Source',
                'multiOptions' => TransactionSource::fetchSelect(TRUE),
            )),
			
			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Save Changes',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
);