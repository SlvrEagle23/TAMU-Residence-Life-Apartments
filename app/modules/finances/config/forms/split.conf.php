<?php
/**
 * Transaction split edit form
 */

return array(	
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
		'elements'		=> array(
            
            'ledger_id'     => array('select', array(
                'label' => 'Ledger Type',
                'required' => true,
                'multiOptions' => Split::getLedgerIds(),
            )),
			
			'credit_or_debit' => array('select', array(
				'label' => 'Credit or Debit',
				'required' => true,
                'multiOptions' => array(
                    'D' => 'Debit',
                    'C' => 'Credit',
                ),
	        )),
			
            'item_id' => array('select', array(
                'label' => 'Item',
                'multiOptions' => Item::fetchSelect(),
            )),
            
            'payment_method_id' => array('select', array(
                'label' => 'Payment Method',
                'multiOptions' => PaymentMethod::fetchSelect(),
            )),
            
            'split_amount' => array('text', array(
                'label' => 'Item Amount',
                'filters' => array('Float'),
                'description' => 'Enter a dollar amount, without the dollar sign (i.e. 0.50)',
            )),
            
            'due_date' => array('unixdate', array(
                'label' => 'Due Date',
                'description' => 'Format: MM/DD/YYYY. Leave blank if not applicable.',
            )),
			
			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Save Changes',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
);