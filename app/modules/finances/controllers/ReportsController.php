<?php
/**
 * Reconciliation report controller.
 */

class Finances_ReportsController extends \DF\Controller\Action
{
    public function permissions()
    {
        return \DF\Acl::getInstance()->isAllowed('view ledgers');
    }
    
    public function indexAction()
    { }
    
    public function reconcileAction()
    {
        $register_id = $this->_getParam('register_id');
        
        if (!$this->_hasParam('start_date') && !$this->_hasParam('end_date') && !$this->_hasParam('date_range'))
        {
            if ((int)$register_id == 0)
            {
                $this->render('reconcile_select_internet');
            }
            else
            {
                $reports_raw = RegisterReport::fetchByRegisterId($register_id);
                
                if (!$reports_raw)
                    throw new \DF\Exception\DisplayOnly('No reports have been submitted for this register!');
                
                $reports = array();
                foreach($reports_raw as $report)
                {
                    $reports[] = $report;
                }
                
                $report_select = array();
                
                // Add special "up to now" report.
                $first_report_array = array_slice($reports, 0, 1);
                $first_report = $first_report_array[0];
                
                $first_report_timestamp = (int)$first_report->timestamp+1;
                $first_report_range = $first_report_timestamp.'_'.time();
                $first_report_name = date('m/d/Y g:ia', $first_report_timestamp).' to Current Time';
                $report_select[$first_report_range] = $first_report_name;
                
                // Produce main list of reports.
                foreach($reports as $i => $report)
                {
                    if (isset($reports[$i+1]))
                    {
                        $next_report = $reports[$i+1];
                        $next_report_timestamp = (int)$next_report->timestamp+1;
                        $next_report_timestamp_name = date('m/d/Y g:ia', $next_report_timestamp);
                    }
                    else
                    {
                        $next_report_timestamp = 0;
                        $next_report_timestamp_name = 'First Report';
                    }
                    
                    $this_report_timestamp = (int)$report->timestamp;
                    $this_report_timestamp_name = date('m/d/Y g:ia', $this_report_timestamp);
                    
                    $timestamp_range = $next_report_timestamp.'_'.$this_report_timestamp;
                    $timestamp_text = $next_report_timestamp_name.' to '.$this_report_timestamp_name;
                    
                    $report_select[$timestamp_range] = $timestamp_text;
                }
                
                $this->view->report_select = $report_select;
                
                $this->render('reconcile_select_register');
            }
        }
        else
        {
            if ($register_id != "all")
                $this->view->register = Register::find($register_id);
            
            // Date calculation.
            list($start_date, $end_date) = $this->_getTimestamps();
            
            $this->view->assign(array(
                'start_date' => $start_date,
                'end_date' => $end_date,
            ));
            
            // Check for register balance reports within the range.
            $reports_q = Doctrine_Query::create()
                ->from('RegisterReport rr')
                ->addWhere('rr.timestamp >= ?', $start_date)
                ->addWhere('rr.timestamp <= ?', $end_date);
            
            if ($register_id != "all")
                $reports_q->addWhere('rr.register_id = ?', $register_id);
            
            $reports = $reports_q->fetchArray();
            $this->view->register_reports = $reports;
            
            // Fetch transactions within that range.
            $items_q = Doctrine_Query::create()
                ->from('Split s')
                ->leftJoin('s.Transaction t')
                ->leftJoin('s.Item i')
                ->leftJoin('i.Account a')
				->leftJoin('s.Asset as')
				->leftJoin('as.AssetLocation al')
                ->addWhere('s.posted >= ?', $start_date)
                ->addWhere('s.posted <= ?', $end_date)
                ->addWhere('s.credit_or_debit = ?', 'C');
            
            if ($register_id != "all")
                $items_q->addWhere('t.register_id = ?', $register_id);
            
            $items = $items_q->fetchArray();
			
            $totals = array();
            $totals_by_acct = array();

            $overall_totals = array(
				'cash' => 0,
				'check' => 0,
				'other' => 0,
				'all' => 0,
			);
            
            if ($items)
            {
                foreach($items as $item)
                {
					if (isset($item['Asset']['AssetLocation']['support_account']))
						$support_account = $item['Asset']['AssetLocation']['support_account'];
					else
						$support_account = 0;
					
					$account_id = $item['Item']['Account']['id'].'_'.$item['Item']['id'];
                    $account_name = $item['Item']['Account']['name'].' - '.$item['Item']['name'];

                    $blank_account_info = array(
                        'name' => $account_name,
                        'totals' => array(
                            'cash' => 0,
                            'check' => 0,
                            'other' => 0,
                        ),
                    );
                
                    if (!isset($totals_by_acct[$account_id]))
                        $totals_by_acct[$account_id] = $blank_account_info;
                    
                    if (!isset($totals[$support_account][$account_id]))
                        $totals[$support_account][$account_id] = $blank_account_info;
					
					if($item['payment_method_id'] == 1)
						$payment_method = 'cash';
					else if ($item['payment_method_id'] == 2)
						$payment_method = 'check';
					else
						$payment_method = 'other';
                    
                    $amt = $item['split_amount'];

                    $totals_by_acct[$account_id]['totals'][$payment_method] += $amt;
                    $totals_by_acct[$account_id]['totals']['all'] += $amt;
					
					$totals[$support_account][$account_id]['totals'][$payment_method] += $amt;
					$totals[$support_account][$account_id]['totals']['all'] += $amt;
					
					$overall_totals[$payment_method] += $amt;
					$overall_totals['all'] += $amt;
                }
            }
			
			// Get all support account names and join them to the list.
			$support_accounts_raw = AssetLocation::fetchArray();
			$support_accounts = array(
				0		=> 'General Charges',
			);
			
			foreach($support_accounts_raw as $location)
			{
				$sup_act = $location['support_account'];
				$support_accounts[$sup_act] = $location['name'].' ('.$sup_act.')';
			}
			
			$report = array();
			foreach($totals as $support_account => $account_totals)
			{
				$report[$support_account] = array(
					'name'		=> $support_accounts[$support_account],
					'totals'	=> $account_totals,
				);
			}
			
            $this->view->report = $report;
            $this->view->totals_by_acct = $totals_by_acct;
            $this->view->overall_totals = $overall_totals;
        }
    }

    public function vacancycostAction()
    {
        $register_id = $this->_getParam('register_id');
        
        if (!$this->_hasParam('start_date') && !$this->_hasParam('end_date') && !$this->_hasParam('date_range'))
        {
            $this->render('vacancycost_select');
        }
        else
        {
            list($start_date, $end_date) = $this->_getTimestamps();
            $this->view->start_date = $start_date;
            $this->view->end_date = $end_date;

            $this->view->vacancies = Asset::fetchVacancyByRange($start_date, $end_date);
        }
    }

    protected function _getTimestamps()
    {
        if ($this->_hasParam('date_range'))
        {
            list($start_date, $end_date) = explode('_', $this->_getParam('date_range'));
        }
        else
        {
            $start_date = strtotime($this->_getParam('start_date'));
            $start_date_array = array(
                'year'      => date('Y', $start_date),
                'month'     => date('m', $start_date),
                'day'       => date('d', $start_date),
            );
            $date = new Zend_Date($start_date_array);
            $start_date = $date->getTimestamp();
            
            $end_date = strtotime($this->_getParam('end_date'));
            $end_date_array = array(
                'year'      => date('Y', $end_date),
                'month'     => date('m', $end_date),
                'day'       => date('d', $end_date)+1,
            );
            $date = new Zend_Date($end_date_array);
            $end_date = $date->getTimestamp()-1;
        }

        return array($start_date, $end_date);
    }
	
	/**
	 * FAMIS charge split report.
	 */
	public function famisAction()
	{
        if (!$this->_hasParam('start_date') && !$this->_hasParam('end_date'))
        {
			$this->render('famis_select');
			return;
		}
		
		$this->view->start_date = $start_date = strtotime($this->_getParam('start_date').' 12:00 am');
		$this->view->end_date = $end_date = strtotime($this->_getParam('end_date').' 11:59 pm');
		
		$transactions = UA_FamisManager::getReportForTimeRange($start_date, $end_date);
		
		$transactions_by_hash = array();
		foreach($transactions as $transaction)
		{
			$hash = $transaction['hash'];			
			$transactions_by_hash[$hash][] = $transaction;
		}
		
		$duplicates = array();
		foreach($transactions_by_hash as $hash => $items)
		{
			if (count($items) > 1)
				$duplicates[] = $items;	
		}
		
		$this->view->duplicates = $duplicates;
		$this->view->transactions = $transactions;
	}
	
	/**
	 * TouchNet Data Reproduction Report
	 */
	public function touchnetAction()
	{
        if (!$this->_hasParam('start_date') && !$this->_hasParam('end_date'))
        {
			$this->render('touchnet_select');
			return;
		}
		
		$this->view->start_date_raw = $start_date_raw = $this->_getParam('start_date');
		$this->view->end_date_raw = $end_date_raw = $this->_getParam('end_date');
		
		$this->view->start_date = $start_date = strtotime($start_date_raw.' 00:00:00');
		$this->view->end_date = $end_date = strtotime($end_date_raw.' 23:59:59');
		
		$transactions = UA_FamisManager::generateReportForTimeRange($start_date, $end_date, FALSE);
		
		if (strtolower($this->_getParam('format')) == "csv")
		{
			$this->doNotRender();
			
			$export_data = array();
			
			$export_header = array(
				'Totals Match',
				'Vendor ID',
				'Transaction ID',
				'Resident ID',
				'Date',
				'Full Amount',
			);
			for($i = 0; $i < 20; $i++)
			{
				$export_header[] = 'Item #'.($i+1).' Account';
				$export_header[] = 'Item #'.($i+1).' Amount';
			}
			
			$export_data[] = $export_header;
			
			foreach($transactions as $trans)
			{
				$export_row = array();
				
				$export_row[] = ($trans['error']) ? 'N' : 'Y';
				$export_row[] = $trans['vendor_id'];
				$export_row[] = $trans['order_id'];
				$export_row[] = $trans['resident_id'];
				$export_row[] = $trans['date'];
				$export_row[] = '$'.number_format($trans['amount'], 2);
				
				for($i = 0; $i < 20; $i++)
				{
					$export_row[] = $trans['items'][$i]['account'];
					$export_row[] = '$'.number_format($trans['items'][$i]['amount'], 2);
				}
				
				$export_data[] = $export_row;
			}
			
			DF_Export::exportToCSV($export_data, TRUE);
			return;
		}
        else
        {
			$this->view->transactions = $transactions;
        }
	}
	
	/* Outstanding charges for current residents report. */
	public function chargesAction()
	{
		$assets_raw = Asset::fetchSelect(FALSE, FALSE);
		
		$assets = array();
		foreach($assets_raw as $asset_id => $asset_name)
		{
			$assets[$asset_id] = array(
				'name'		=> $asset_name,
				'occupancies' => array(),
			);
		}
		
		// Get all active occupants.
        $occupancies = Doctrine_Query::create()
            ->from('Occupancy o')
			->innerJoin('o.Resident r')
			->innerJoin('o.Asset a')
            ->addWhere('o.start_date <= ?', time())
            ->addWhere('o.end_date IS NULL OR o.end_date = 0 OR o.end_date >= ?', time())
			->orderBy('a.name ASC')
            ->execute(array(), Doctrine_Core::HYDRATE_ARRAY);
		
		$total = 0.0;
        $totals_by_acct = array();
		
		foreach($occupancies as $occupancy)
		{
			$asset_id = $occupancy['asset_id'];
			$resident_id = $occupancy['resident_id'];
			
			$occupancy['balance'] = Split::getBalanceByResidentId($resident_id, Split::LEDGER_STANDARD);
			// $total += $occupancy['balance'];
            
            if ($occupancy['balance'] < 0)
            {
                $assets[$asset_id]['occupancies'][] = $occupancy;

                // Fetch transactions within that range.
                $items = Doctrine_Query::create()
                    ->from('Split s')
                    ->leftJoin('s.Transaction t')
                    ->leftJoin('s.Item i')
                    ->leftJoin('i.Account a')
                    ->addWhere('s.resident_id = ?', $resident_id)
                    ->addWhere('s.ledger_id = ?', Split::LEDGER_STANDARD)
                    ->addWhere('s.matching_split_id IS NULL')
                    ->addWhere('s.credit_or_debit = ?', 'D')
                    ->fetchArray();
                
                foreach($items as $item)
                {
                    $account_id = $item['Item']['Account']['id'].'_'.$item['Item']['id'];
                    $account_name = $item['Item']['Account']['name'].' - '.$item['Item']['name'];

                    if (!isset($totals_by_acct[$account_id]))
                    {
                        $totals_by_acct[$account_id] = array(
                            'name' => $account_name,
                            'total' => 0,
                        );
                    }

                    $totals_by_acct[$account_id]['total'] += $item['split_amount'];
                    $total += $item['split_amount'];
                }
            }
		}
		
		$this->view->assets = $assets;
        $this->view->totals_by_acct = $totals_by_acct;
		$this->view->total = $total;
	}
    
    /* Outstanding charges for non-residents report. */
    public function nonresidentchargesAction()
    {
        // Find all residents to exclude from list.
        $occupancies = Doctrine_Query::create()
            ->select('r.id')
            ->from('Resident r')
            ->leftJoin('r.Occupancies o')
            ->addWhere('o.start_date <= ?', time())
            ->addWhere('o.end_date IS NULL OR o.end_date = 0 OR o.end_date >= ?', time())
            ->fetchArray();
        
        $resident_ids = array();
        foreach($occupancies as $occupancy)
        {
            $resident_ids[] = $occupancy['id'];
        }
        
        // Find all non-residents.
        $all_nonresidents = Doctrine_Query::create()
            ->select('r.*')
            ->from('Resident r')
            ->whereNotIn('r.id', $resident_ids)
            ->orderBy('r.surname ASC, r.first_name ASC')
            ->fetchArray();
        
        $nonresidents = array();
        
        foreach($all_nonresidents as $resident)
        {
            $balance = Split::getBalanceByResidentId($resident['id'], Split::LEDGER_STANDARD);
            
            if ($balance != 0)
            {
                $resident['balance'] = $balance;
                $nonresidents[] = $resident;
            }
        }
        
        switch(strtolower($this->_getParam('format', 'html')))
        {
            case "csv":
                $this->doNotRender();
                
                $export_data = array(array(
                    'UIN',
                    'First Name',
                    'Last Name',
                    'Balance Due',
                ));
                
                foreach($nonresidents as $resident)
                {
                    $export_row = array(
                        $resident['uin'],
                        $resident['first_name'],
                        $resident['surname'],
                        '$'.number_format($resident['balance'], 2)
                    );
                    $export_data[] = $export_row;
                }
                
                DF_Export::exportToCSV($export_data, TRUE);
                return;
            break;
            
            case "html":
            default:
                $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($nonresidents));
                $paginator->setCurrentPageNumber(($this->_hasParam('page')) ? $this->_getParam('page') : 1);
                $this->view->pager = $paginator;
                
                $this->render();
                return;
            break;
        }
    }
    
    /* Preliminary electric billing report. */
    public function electricprelimAction()
    {
        $report_raw = UA_ElectricManager::generateCharges(NULL, TRUE);

        $report_by_location = array();
        $unbilled = array();

        foreach((array)$report_raw as $report_item)
        {
            if ($report_item['is_billed'])
            {
                $report_by_location[$report_item['asset_location']][] = $report_item;
            }
            else
            {
                $unbilled[] = $report_item;
            }
        }

        $this->view->report = $report_by_location;
        $this->view->unbilled = $unbilled;
    }
}