<?php
class Finances_RefundController extends DF_Controller_Action
{
    public function permissions()
    {
        return DF_Acl::getInstance()->isAllowed('create refunds');
    }

    /* Main display. */
    public function indexAction()
    {
        $this->redirectFromHere(array('action' => 'deposits'));
    }
    
    /* Refund security deposits. */
    public function depositsAction()
    {
        $user = DF_Auth::getInstance()->getLoggedInUser();
        
        $id = $this->_getParam('id');
        $resident = Resident::fetchById($id);

        $final_charge_names_raw = array('Final Utilities', 'Repair/Maintenance');
        $final_charge_types = array();
        $final_charge_names = array();

        foreach($final_charge_names_raw as $charge_name)
        {
            $item = Item::fetchByName($charge_name);

            $final_charge_types[$item->id] = $item;
            $final_charge_names[$item->id] = $item->name;
        }

        // Post any charges specified in the form.
        if ($this->_hasParam('final_charges'))
        {
            $final_transaction = new RegisterTransaction();
            $final_transaction->posted = time();
            $final_transaction->date = time();
            $final_transaction->memo = 'Final Charges';
            $final_transaction->save();

            $final_charges = (array)$this->_getParam('final_charges');
            foreach($final_charges as $item_id => $amount)
            {
                $debit_item = new Split();
                $debit_item->transaction_id = $final_transaction->id;
                $debit_item->resident_id = $id;
                $debit_item->user_id = $user->id;
                $debit_item->ledger_id = Split::LEDGER_STANDARD;
                $debit_item->credit_or_debit = 'D';
                $debit_item->split_amount = 0-abs($amount);
                $debit_item->item_id = $final_charge_items[$item_id];
                $debit_item->save();
            }
        }
        
        // Get pending items for both ledger types.
        $deposit_items = Split::fetchByResidentId($id, Split::LEDGER_DEPOSITS);
        $deposit_total = Split::getBalanceByResidentId($id, Split::LEDGER_DEPOSITS);
        
        $debit_items = Split::fetchByResidentId($id, Split::LEDGER_STANDARD);
        $debit_total = Split::getBalanceByResidentId($id, Split::LEDGER_STANDARD);
        
        $refund_amount = $deposit_total + $debit_total;
        
        $this->view->assign(array(
            'resident'          => $resident,
            'deposit_total'     => $deposit_total,
            'debit_total'       => $debit_total,
            'refund_amount'     => $refund_amount,
            'final_charge_names' => $final_charge_names,
        ));
        
        if ($this->_hasParam('process'))
        {
            $transaction = new RegisterTransaction();
            $transaction->posted = time();
            $transaction->date = time();
            $transaction->memo = 'Security Deposit Refund';
            $transaction->save();
            
            $transaction_id = $transaction->id;

            $deposit_item = Item::fetchByName('Security Deposit');
            
            // Create security deposit charges.
            if ($deposit_items->count() > 0)
            {
                foreach($deposit_items as $deposit_item)
                {
                    if (!$deposit_item->matching_split_id)
                    {
                        $deposit_charge = new Split();
                        $deposit_charge->transaction_id = $transaction_id;
                        $deposit_charge->resident_id = $id;
                        $deposit_charge->user_id = $user->id;
                        $deposit_charge->ledger_id = Split::LEDGER_DEPOSITS;
                        $deposit_charge->credit_or_debit = 'D';
                        $deposit_charge->split_amount = 0-$deposit_item->split_amount;
                        $deposit_charge->item_id = $deposit_item;
                        $deposit_charge->matching_split_id = $deposit_item->id;
                        $deposit_charge->save();
                        
                        $deposit_item->matching_split_id = $deposit_charge->id;
                        $deposit_item->save();
                    }
                }
            }
            
            // Create debit counterpayment.
            if ($debit_items->count() > 0)
            {
                foreach($debit_items as $debit_item)
                {
                    if (!$debit_item->matching_split_id)
                    {
                        $debit_charge = new Split();
                        $debit_charge->transaction_id = $transaction_id;
                        $debit_charge->resident_id = $id;
                        $debit_charge->user_id = $user->id;
                        $debit_charge->ledger_id = Split::LEDGER_STANDARD;
                        $debit_charge->credit_or_debit = 'C';
                        $debit_charge->split_amount = 0-$debit_item->split_amount;
                        $debit_charge->item_id = Item::fetchByName('Security Deposit');
                        $debit_charge->matching_split_id = $debit_item->id;
                        $debit_charge->save();
                        
                        $debit_item->matching_split_id = $debit_charge->id;
                        $debit_item->save();
                    }
                }
            }
            
            $this->render('deposits_success');
            return;
        }
    }
}