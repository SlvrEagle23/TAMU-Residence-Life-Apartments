<?php
class Finances_TransactionController extends \DF\Controller\Action
{
    public function permissions()
    {
		if ($this->_getParam('action') == "rollback")
			return \DF\Acl::getInstance()->isAllowed('create debits');
		else
        	return \DF\Acl::getInstance()->isAllowed('manage transactions');
    }

    /**
     * Main display.
     */
    public function indexAction()
    {
		$query = Doctrine_Query::create()->from('RegisterTransaction rt')->orderBy('id DESC');
		
		$paginator = new DF_Paginator_Doctrine($query);
        $paginator->setCurrentPageNumber(($this->_hasParam('page')) ? $this->_getParam('page') : 1);
		$this->view->pager = $paginator;
    }
    
    /**
     * View transaction
     */
    public function viewAction()
    {
        $id = (int)$this->_getParam('id');
        
        $record = RegisterTransaction::find($id);
        if (!$record)
            throw new \DF\Exception\DisplayOnly('Transaction not found!');
        
        $this->view->transaction = $record;
    }
	
	/**
	 * Rollback a transaction (i.e. TouchNet NSF reversal)
	 */
	public function rollbackAction()
	{
		if (!$this->_hasParam('id'))
		{
			$this->render();
			return;
		}
		else
		{
			$touchnet = Touchnet::fetchByOrderId($this->_getParam('id'));
			
			if (!$touchnet)
				throw new \DF\Exception\DisplayOnly('The transaction you entered does not exist in the system. Please go back and enter a new transaction number.');
			
			$resident_id = $touchnet->local_record_id;
			$resident = Resident::find($resident_id);
			
			if (!$this->_hasParam('confirm'))
			{
				$this->view->touchnet = $touchnet;
				$this->view->resident = $resident;
				$this->view->id = $this->_getParam('id');
				
				$this->render('rollback_confirm');
				return;
			}
			else
			{
				$amount_paid = $touchnet->payment_amount;
				$add_fee = ($this->_getParam('add_fee') == 1);
                
                if ($add_fee)
                {
                    // Process a NSF charge and fee.
                    $transaction = new RegisterTransaction();
                    $transaction->posted = time();
                    $transaction->date = time();
                    $transaction->transaction_type = RegisterTransaction::TRANSACTION_TYPE_MANUAL;
                    $transaction->memo = 'Charges from Reversed Online Payment (T'.$touchnet->payment_order_id.')';
                    $transaction->save();
                    
                    $transaction_id = $transaction->id;
                    
                    $split = new Split();
                    $split->posted = time();
                    $split->transaction_id = $transaction_id;
                    $split->resident_id = $resident->id;
                    $split->ledger_id = Split::LEDGER_STANDARD;
                    $split->credit_or_debit = 'D';
                    $split->item_id = Item::fetchByName('NSF Payment');
                    $split->split_amount = 0-$amount_paid;
                    $split->save();
                    
					$split2 = new Split();
					$split2->posted = time();
					$split2->transaction_id = $transaction_id;
					$split2->resident_id = $resident->id;
					$split2->ledger_id = Split::LEDGER_STANDARD;
					$split2->credit_or_debit = 'D';
					$split2->item_id = Item::fetchByName('NSF Check Fee');
					$split2->split_amount = 0-(30.00);
					$split2->save();
                }
                else
                {
                    // Process a NSF charge and fee.
                    $transaction = new RegisterTransaction();
                    $transaction->posted = time();
                    $transaction->date = time();
                    $transaction->transaction_type = RegisterTransaction::TRANSACTION_TYPE_MANUAL;
                    $transaction->memo = 'Charges from Reversed Online Payment (T'.$touchnet->payment_order_id.')';
                    $transaction->save();
                    
                    // Reverse existing charges, but post no fee.
                    $touchnet = Doctrine_Query::create()
                        ->from('Touchnet t')
                        ->leftJoin('t.Splits s')
                        ->addWhere('(s.deleted_at = s.deleted_at OR s.deleted_at IS NULL)') // Include deleted splits.
                        ->addWhere('t.id = ?', $touchnet->id)
                        ->fetchOne();
                    
                    $splits_to_update = array();
                    foreach($touchnet->Splits as $split)
                    {
                        if ($split->matching_split_id)
                        {
                            $matching_split = Split::find($split->matching_split_id);
                            if ($matching_split instanceof Split)
                            {
                                $matching_trans = $matching_split->Transaction;
                                $matching_trans->memo = 'Online Payment (Reversed - ACH Error)';
                                $matching_trans->save();
                            }
                        }
                        
                        $split_info = array(
                            'ledger_id'     => (int)$split->ledger_id,
                            'resident_id'   => $split->resident_id,
                            'credit_or_debit' => 'D',
                            'user_id'       => $split->user_id,
                            'item_id'       => $split->item_id,
                            'asset_id'      => $split->asset_id,
                            'posted'        => time(),
                            'split_amount'  => $split->split_amount,
                            'transaction_id' => $transaction->id,
                        );
                        
                        try
                        {
                            $new_split = new Split();
                            $new_split->fromArray($split_info);
                            $new_split->save();
                        }
                        catch(Exception $e)
                        {
                            \DF\Utilities::print_r($split_info);
                            exit;
                        }
                    }
                }
				
				Split::updateRunningBalance($resident->id, Split::LEDGER_STANDARD);
				Split::updateRunningBalance($resident->id, Split::LEDGER_DEPOSITS);
				
				$this->alert('<b>Transaction rolled back.</b><br />Any changes to the resident\'s ledger associated with this transaction have been reversed.');
                
				$this->redirectToRoute(array('module' => 'finances', 'controller' => 'transaction', 'action' => 'rollback'));
			}
		}
	}
    
    /* Edit transaction */
    public function editAction()
    {
        $form = new \DF\Form($this->current_module_config->forms->transaction->form);
        
        $id = intval($this->_getParam('id'));
        if ($id != 0)
        {
            $record = RegisterTransaction::find($id);
            $form->setDefaults($record->toArray());
        }
        
        if( !empty($_POST) && $form->isValid($_POST) )
        {
            $data = $form->getValues();
            
            if (!$record)
            {
                $record = new RegisterTransaction();
                $record->posted = time();
            }
            
            $record->date = $data['date'];
            $record->memo = $data['memo'];
            $record->check_reference_number = $data['check_reference_number'];
            $record->no_sale = $data['no_sale'];
            $record->reference_number = $data['reference_number'];
            
			if ($data['register_id'] === "null")
                unset($record->register_id);
            else
                $record->register_id = $data['register_id'];
            
			if ($data['transaction_source_id'] === "null")
                unset($record->transaction_source_id);
            else
                $record->transaction_source_id = $data['transaction_source_id'];
            
            $record->save();
            
            $this->alert('Transaction changes saved.');
            $this->redirectFromHere(array('action' => 'view', 'id' => $record->id));
        }
        
        $this->view->form = $form;
    }
    
    public function deleteAction()
	{
		$record = RegisterTransaction::find($this->_getParam('id'));
        
        if ($record)
        {
            if ($record->Splits->count() > 0)
            {
                foreach($record->Splits as $split)
                {
                    $split->delete();
                }
            }
            
            $record->delete();
        }
        
        $this->alert('Record deleted.');
        $this->redirectFromHere(array('action' => 'index', 'id' => NULL));
	}
    
    /* Add/edit transaction split */
    public function editsplitAction()
    {
        $form = new \DF\Form($this->current_module_config->forms->split->form);
        
        $id = (int)$this->_getParam('id');
        $split_id = (int)$this->_getParam('split_id');
        
        if ($split_id != 0)
        {
            $record = Split::find($split_id);
            
            // Check that split isn't recurring.
            if ($record->recurring_parent_id)
            {
                $split_id = $record->recurring_parent_id;
                $new_url = \DF\Url::route(array('split_id' => $split_id), NULL, FALSE);
                
                throw new \DF\Exception\DisplayOnly('This charge is part of a recurring transaction. To change it, you must edit the main recurring item. <a href="'.$new_url.'">Click here to go to the main item</a>.');
            }
            
            $form->setDefaults($record->toArray());
        }
        
        if( !empty($_POST) && $form->isValid($_POST) )
        {
            $data = $form->getValues();
            
            if (!$record)
            {
                $record = new Split();
                $record->transaction_id = (int)$id;
                
                if ($this->_hasParam('resident_id'))
                    $record->resident_id = $this->_getParam('resident_id');
            }
            
            $record->ledger_id = (int)$data['ledger_id'];
            $record->credit_or_debit = $data['credit_or_debit'];
            $record->item_id = (int)$data['item_id'];
            $record->payment_method_id = $data['payment_method_id'];
            $record->due_date = $data['due_date'];
            $record->split_amount = $data['split_amount'];
            
            try
            {
                $record->save();
                Split::updateRunningBalance($record->resident_id, $record->ledger_id);
                
                $this->flash('Transaction item changes saved.<br><br><b>Note: If you made changes to the amount of this transaction, don\'t forget to note the reason for the change in the transaction memo. Click "Edit Transaction" below to set the memo.</b>');
            }
            catch(Doctrine_Exception $e)
            {
                $this->alert($e->getMessage());
            }
            $this->redirectFromHere(array('action' => 'view', 'id' => $id));
        }
        
        $this->view->form = $form;
    }
    
    public function deletesplitAction()
	{
		$record = Split::find($this->_getParam('split_id'));
        if ($record)
            $record->delete();
        
        $this->alert('Item deleted.');
        $this->redirectFromHere(array('action' => 'view', 'id' => $this->_getParam('id'), 'split_id' => NULL));
	}
}