<?php
return array(
    'default' => array(
        'finances'  => array(
            'label' => 'Finances',
            'module' => 'finances',
            'controller' => 'index',
            'permission' => 'is logged in',
            
            'pages' => array(
                'resident_history' => array(
                    'label' => 'Payment History',
                    'module' => 'finances',
                    'controller' => 'resident',
                    'action' => 'ledger',
                    'permission' => 'is logged in',
                ),
                'pay' => array(
                    'label' => 'Pay Bill Online',
                    'module' => 'finances',
                    'controller' => 'resident',
                    'action' => 'pay',
                    'permission' => 'is logged in',
                ),
                
                'ledger' => array(
                    'module' => 'finances',
                    'controller' => 'ledger',
                    'action' => 'index',
                ),
                'transaction_post' => array(
                    'module' => 'finances',
                    'controller' => 'ledger',
                    'action' => 'post',
                ),
                
                'manage_transactions' => array(
                    'label' => 'Manage Transactions',
                    'module' => 'finances',
                    'controller' => 'transaction',
                    'action' => 'index',
                    'permission' => 'administer all',
                    
                    'pages' => array(
                        'transaction_view' => array(
                            'module' => 'finances',
                            'controller' => 'transaction',
                            'action' => 'view',
                        ),
                        'transaction_edit' => array(
                            'module' => 'finances',
                            'controller' => 'transaction',
                            'action' => 'edit',
                        ),
                        'transaction_editsplit' => array(
                            'module' => 'finances',
                            'controller' => 'transaction',
                            'action' => 'editsplit',
                        ),
                    ),
                ),
                
                'reports' => array(
                    'label' => 'Reports',
                    'module' => 'finances',
                    'controller' => 'reports',
                    'action' => 'index',
                    'permission' => 'view ledgers',
                    
                    'pages' => array(
                        'reconcile' => array(
                            'label' => 'Reconciliation Report',
                            'module' => 'finances',
                            'controller' => 'reports',
                            'action' => 'reconcile',
                            'permission' => 'view ledgers',
                        ),
                    ),
                ),
                
                'blocked_residents' => array(
                    'label' => 'View Blocked Residents',
                    'module' => 'finances',
                    'controller' => 'resident',
                    'action' => 'blocked',
                    'permission' => 'view ledgers',
                ),
            ),
        ),
    ),
);
