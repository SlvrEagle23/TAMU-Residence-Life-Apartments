<?php
function _check_dev_env()
{
    if (DF_APPLICATION_ENV == "development")
        return true;
    else
        return false;
}

return array(
    'default' => array(
        'admin' => array(
            'label' => 'Administer',
            'module' => 'admin',
            'show_only_with_subpages' => TRUE,
			
            'order' => 5,
            'pages' => array(
            
				'settings'	=> array(
					'label'	=> 'Site Settings',
					'module' => 'admin',
					'controller' => 'settings',
					'action' => 'index',
					'permission' => 'administer all',
				),
				
				'blocks'	=> array(
					'label'	=> 'Content Blocks',
					'module' => 'admin',
					'controller' => 'blocks',
					'action' => 'index',
					'permission' => 'administer blocks',
				),
				'leases'	=> array(
					'label'	=> 'Lease Versions',
					'module' => 'admin',
					'controller' => 'leases',
					'permission' => 'administer blocks',
				),
				
				'users' => array(
					'label' => 'Users',
					'module' => 'admin',
					'controller' => 'users',
					'action' => 'index',
					'permission' => 'administer users',
				),
				
				'permissions' => array(
					'label' => 'Permissions',
					'module' => 'admin',
					'controller' => 'permissions',
					'permission' => 'manage permissions',
				), 

				'residenttypes' => array(
					'label' => 'Resident Types',
					'module' => 'admin',
					'controller' => 'residenttypes',
				),
				'assettypes' => array(
					'label' => 'Asset Types',
					'module' => 'admin',
					'controller' => 'assettypes',
				),
            ),
        ),
    ),
);