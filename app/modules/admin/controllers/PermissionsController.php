<?php
use \Entity\Action;
use \Entity\Role;

class Admin_PermissionsController extends \DF\Controller\Action
{
    public function permissions()
    {
        return $this->acl->isAllowed('manage permissions');
    }
    
    public function indexAction()
    {
    	$this->view->actions = Action::fetchArray('name');
    	$this->view->roles = Role::fetchArray('name');
    }
	
	public function editactionAction()
	{
        $form = new \DF\Form($this->current_module_config->forms->action->form);
		
		if ($this->_hasParam('id'))
		{
			$record = Action::find($this->_getParam('id'));
			$form->setDefaults($record->toArray());
		}

        if(!empty($_POST) && $form->isValid($_POST))
        {
            $data = $form->getValues();
			
			if (!($record instanceof Action))
				$record = new Action();
			
			$record->fromArray($data);
			$record->save();
			
			$this->alert('Action updated.');
            $this->redirectFromHere(array('action' => 'index', 'id' => NULL, 'csrf' => NULL));
            return;
        }

        $this->view->headTitle('Add/Update Action');
        $this->renderForm($form);
	}
	
	public function deleteactionAction()
	{
        $this->validateToken($this->_getParam('csrf'));
		
		$action = Action::find($this->_getParam('id'));
		if ($action)
			$action->delete();
			
		$this->alert('Action deleted!');
        $this->redirectFromHere(array('action' => 'index', 'id' => NULL, 'csrf' => NULL));
	}

    public function editroleAction()
    {
		$form_config = $this->current_module_config->forms->role->form->toArray();
        $form = new \DF\Form($form_config);
		
		if ($this->_hasParam('id'))
		{
			$record = Role::find($this->_getParam('id'));
			$form->setDefaults($record->toArray(TRUE, TRUE));
		}

        if( !empty($_POST) && $form->isValid($_POST) )
        {
            $data = $form->getValues();
			
			if (!($record instanceof Role))
				$record = new Role();

			$record->fromArray($data);
			$record->save();

			$this->alert('Role updated.', 'green');
			$this->redirectFromHere(array('action' => 'index', 'id' => NULL, 'csrf' => NULL));
			return;
        }

        $this->view->headTitle('Add/Update Role');
        $this->renderForm($form);
    }

    public function deleteroleAction()
    {
        $this->validateToken($this->_getParam('csrf'));
		
		$record = Doctrine_Core::getTable('Role')->find($this->_getParam('id'));
		
		if ($record)
		{
			$delete_actions = Doctrine_Query::create()
				->delete('RoleHasAction ra')
				->addWhere('ra.role_id = ?', $record->id)
				->execute();
			
			$delete_users = Doctrine_Query::create()
				->delete('UserHasRole ur')
				->addWhere('ur.role_id = ?', $record->id)
				->execute();
			
			$record->delete();
		}
		
		$this->alert("Role deleted!");
		$this->redirectFromHere(array('action' => 'index', 'id' => NULL, 'csrf' => NULL));
    }
}