<?php
use \Entity\Asset;
use \Entity\AssetType;
use \Entity\AssetCost;

class Admin_AssetcostsController extends \DF\Controller\Action
{
    public function permissions()
    {
        return $this->acl->isAllowed('manage asset costs');
    }
    
    public function indexAction()
    {
        if ($this->_hasParam('asset_id'))
        {
            $asset = Asset::find($this->_getParam('asset_id'));
			
			$this->view->asset = $asset;
			$this->view->asset_id = $asset->id;
			$this->view->asset_records = $asset->costs;
			
			$this->view->asset_type = $asset->type;
			$this->view->asset_type_id = $asset->type->id;			
			$this->view->asset_type_records = $asset->type->costs;
        }
        else if ($this->_hasParam('assettype_id'))
        {
            $asset_type = AssetType::find($this->_getParam('assettype_id'));
			
			$this->view->asset_type = $asset_type;
			$this->view->asset_type_id = $asset_type->id;
			$this->view->asset_type_records = $asset_type->costs;
        }
        else
        {
            throw new \DF\Exception\DisplayOnly('No asset ID or asset type ID found! You cannot manage costs without providing one of these IDs.');
        }
    }
	
    public function editAction()
    {
        $id = (int)$this->_getParam('id');
		$form = new \DF\Form($this->current_module_config->forms->assetcost->form);
        
        $record = AssetCost::find($id);
        if ($record)
            $form->setDefaults($record->toArray());
        
		if(!empty($_POST) && $form->isValid($_POST))
		{
            $data = $form->getValues();
            
            if (!$record)
            {
                $record = new AssetCost();
                
                if ($this->_hasParam('asset_id'))
                    $record->asset_id = (int)$this->_getParam('asset_id');
                else
                    $record->asset_type_id = (int)$this->_getParam('assettype_id');
            }

            $record->fromArray($data);
            $record->save();
            
            $this->alert('Record updated!', 'green');
            $this->redirectFromHere(array('action' => 'index', 'id' => NULL));
            return;
		}

        $this->view->headTitle('Add/Update Asset Cost');
        $this->renderForm($form);
    }
	
	public function deleteAction()
	{
        $this->validateToken($this->_getParam('csrf'));
		
		$record = AssetCost::find($this->_getParam('id'));
		if ($record)
			$record->delete();
		
		$this->alert('Record deleted!');
		$this->redirectFromHere(array('action' => 'index', 'id' => NULL, 'csrf' => NULL));
	}
}