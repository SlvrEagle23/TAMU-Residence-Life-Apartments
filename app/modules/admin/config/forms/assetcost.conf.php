<?php
/**
 * Asset cost form
 */

return array(	
	/**
	 * Form Configuration
	 */
	 
	'form' => array(
		'method'		=> 'post',
		'elements'		=> array(
            
			'cost'		=> array('text', array(
				'label' => 'Cost',
                'filters' => array('Float'),
                'validators' => array('Float'),
	        )),
			
			'start_date' => array('unixdate', array(
				'label' => 'Start Date',
				'required' => true,
	        )),
            
            'end_date' => array('unixdate', array(
				'label' => 'End Date',
                'description' => 'Leave blank for currently active costs with no set end date.',
	        )),
            
            'period' => array('select', array(
                'label' => 'Billing Period',
                'multiOptions' => array('monthly' => 'Monthly'),
            )),
            
			'submit'		=> array('submit', array(
				'type'	=> 'submit',
				'label'	=> 'Save Changes',
				'helper' => 'formButton',
				'class' => 'ui-button',
			)),
		),
	),
); 