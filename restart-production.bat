@ECHO OFF

ECHO Restarting application pool...

SET appName="Apartments"
set serverName="DSAWEB4"

SET deployBinary="C:\Program Files (x86)\IIS\Microsoft Web Deploy V2\msdeploy.exe"
%deployBinary% -verb:sync -source:recycleApp="%appName%",recycleMode="RecycleAppPool",wmsvc="%serverName%",authType="NTLM" -dest:auto,computerName="%serverName%" -allowUntrusted